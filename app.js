/**
 * Module dependencies.
 */

var express = require('express'),
    route = require('./routes'),
    https = require('https'),
    http = require('http'),
    path = require('path'),
    fs = require('fs'),
    app_config = require('./config/app.js'),
    FB_config = require('./config/facebook.js'),
    passport = require('passport');
    //FB = require('fbgraph');

var options = {
    key: fs.readFileSync('ssl.key'),
    cert: fs.readFileSync('ssl.crt'),
    passphrase: 'grassshrimpliu'
}

var app = express();

// all environments
app.set('port', process.env.PORT || app_config.port);
app.set('env', app_config.env);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
//app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser('grassshrimp'));
app.use(express.session({secret: 'grassshrimp'}));
app.use(passport.initialize());
app.use(passport.session());
app.use(app.router);
app.use(express.compress());
app.use(express.static(path.join(__dirname, 'app')));

// development only

if ('development' == app.get('env')) {
    app.use(express.errorHandler());
}

route(app);

if('production' == app.get('env')){
    http.createServer(app).listen(8080, function(){
       console.log('Express Http server listening on port ' + 8080);
    });
}
var https_server = https.createServer(options, app).listen(app.get('port'), function () {
    console.log('Express Https server listening on port ' + app.get('port'));
});

var WebSocketServer = require('./WebSocketServer').listen(https_server);
