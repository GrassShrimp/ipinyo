module.exports = {
    redis: (function(){
        if(process.env.VCAP_SERVICES){
            var env = JSON.parse(process.env.VCAP_SERVICES);
            var redis = env['redis-2.2'][0]['credentials'];
        }
        else {
            var redis = {
                "hostname":"127.0.0.1",
                "host":"127.0.0.1",
                "port":6379,
                "password":"",
                "name":""
            };
        }

        return redis;
    }())
}