/*
 * GET home page.
 */

var middleware = require('../middleware'),
    FB_config = require('../config/facebook.js'),
    FacebookStrategy = require('passport-facebook').Strategy,
    passport = require('passport');

passport.serializeUser(function(id, done) {
    done(null, id);
});

passport.deserializeUser(function(id, done) {
    done(null, id);
});

passport.use(new FacebookStrategy({
        clientID: FB_config.appId,
        clientSecret: FB_config.secret,
        callbackURL: FB_config.callbackURL
    },
    function(accessToken, refreshToken, profile, done) {
        //FB.setAccessToken(accessToken);
        done(null,profile.id);
    }
));

module.exports = function(app){
    app.get('/auth/facebook', passport.authenticate('facebook',{ scope: FB_config.scope }));

    app.get('/auth/facebook/callback', passport.authenticate('facebook', { successRedirect: '/#/', failureRedirect: '/' }));

    app.get('/logout', function(req, res){
        req.logout();
        res.redirect('/');
    });

    app.get('/',
        middleware.checkLogin,
        middleware.checkBrowser,
        function(req,res,next){
            if (!req.secure) {
                return res.redirect('https://' + req.get('host') + req.url);
            }
            next();
        },
        function(req, res){
            res.sendfile('app/dist/index.html');
        }
    );
}
