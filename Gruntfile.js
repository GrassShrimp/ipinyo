'use strict';

module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    concat: {
      options: {
        separator: ';'
      },
      dist: {
        src: ['app/scripts/**/*.js'],
        dest: 'app/dist/app.js'
      }
    },
    uglify: {
      dist: {
        files: {
          'app/dist/app.min.js': ['<%= concat.dist.dest %>']
        }
      }
    },
    jshint: {
      files: ['gruntfile.js', 'app/scripts/**/*.js'],
      options: {
        // options here to override JSHint defaults
        globals: {
          jQuery: true,
          console: true,
          module: true,
          document: true,
          '$': true,
          angular: true,
          navigator: true,
          window: true,
          _: true,
          Firebase: true,
          location: true,
          io: true,
          FB: true,
          Image: true,
          imagesLoaded: true,
          Me: true
        }
      }
    },
    less: {
      production: {
        options: {
          yuicompress: true
        },
        files: {
          "app/dist/app.min.css": ["app/styles/index.less","app/styles/team.less","app/styles/global.less","app/styles/circularG.less","app/styles/f_circleG.less","app/styles/facebookG.less","app/styles/floatingBarsG.less"],
          "app/dist/login.min.css": ["app/styles/login.less"],
          "app/dist/forbidden.min.css": ["app/styles/forbidden.less"]
        }
      }
    },
    htmlmin: {
      dist: {
        options: {
          removeComments: true,
          collapseWhitespace: true
        },
        files: {
          'app/dist/index.html': 'app/index.html',
          'app/dist/Index.html': 'app/views/Index.html',
          'app/dist/Team.html': 'app/views/Team.html',
          'app/dist/CreateTeamModal.html': 'app/views/CreateTeamModal.html',
          'app/dist/FBAlbums.html': 'app/views/FBAlbums.html',
          'app/dist/FBFriends.html': 'app/views/FBFriends.html',
          'app/dist/FBPhotos.html': 'app/views/FBPhotos.html',
          'app/dist/FBVideos.html': 'app/views/FBVideos.html',
          'app/dist/Events.html': 'app/views/Events.html',
          'app/dist/IdeaModal.html': 'app/views/IdeaModal.html',
          'app/dist/NoticeModal.html': 'app/views/NoticeModal.html',
          'app/dist/circularG.html': 'app/views/circularG.html',
          'app/dist/f_circleG.html': 'app/views/f_circleG.html',
          'app/dist/facebookG.html': 'app/views/facebookG.html',
          'app/dist/floatingBarsG.html': 'app/views/floatingBarsG.html',
          'app/dist/login.html':'app/login.html',
          'app/dist/forbidden.html':'app/forbidden.html',
          'app/dist/PrivacyPolicyModal.html': 'app/views/PrivacyPolicyModal.html',
          'app/dist/SendMailModal.html': 'app/views/SendMailModal.html',
          'app/dist/MyYoutube.html': 'app/views/MyYoutube.html',
          'app/dist/PrivacyPolicy.html': 'app/PrivacyPolicy.html',
          'app/dist/EventDetails.html': 'app/views/EventDetails.html',
          'app/dist/CreateEventModal.html': 'app/views/CreateEventModal.html'
        }
      }
    }
  });
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-htmlmin');

  grunt.registerTask('test', ['jshint']);
  grunt.registerTask('default', ['less','htmlmin','jshint', 'concat', 'uglify']);
};
