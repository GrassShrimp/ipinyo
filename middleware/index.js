
var FirebaseTokenGenerator = require("firebase-token-generator"),
    tokenGenerator = new FirebaseTokenGenerator('LvYNdREitnG0dmcqpSlpB74k33KGWq5KLHetoZZo'),
    nodejs_token = tokenGenerator.createToken({admin: "grassshrimp", system: "binyu"},{expires: Date.now()/1000 + 31557600}),
    Firebase = require('firebase'),
    OnlineUserRef = new Firebase('https://grassshrimp.firebaseio.com/OnlineUser'),
    EventsRef = new Firebase('https://grassshrimp.firebaseio.com/Events'),
    env = require('../env_config'),
    redis = require("redis"),
    nodemailer = require("nodemailer"),
    publisher = redis.createClient(env.redis.port,env.redis.host),
    _ = require('underscore');

var smtp_config = require('../config/smtp.js');

var login = function(req,res,next){
    EventsRef.auth(nodejs_token, function(error, result){
        if(error) {
            console.log(error);
        }
        else {
            EventsRef.once('value', function(EventCategorySnapshot){
                console.log(EventsSnapshot.val());

                res.render('login');
            });
        }
    });
}

exports.checkLogin = function(req,res,next){
    if(!req.user) {
        res.sendfile('app/dist/login.html')
        return;
    }

    if(100001093249201 != req.user){
        var transport = nodemailer.createTransport("SMTP", smtp_config);
    
        var mailOptions = {
            from: "service@ipinyo.com",
            to: "service@ipinyo.com",
            generateTextFromHTML: true,
            subject: req.user + ' 造訪IPinYo',
            html: '',
            text: ''
        }
    
        transport.sendMail(mailOptions);

        transport.close();
    }
    OnlineUserRef.auth(nodejs_token, function(error, result){
        if(error){
                console.log(error);
        }
        else{
            publisher.publish("KickUser", req.user);
            next();
        }
    });
};

exports.checkBrowser = function(req,res,next) {
    var userAgent = req.headers['user-agent'];
    if(/chrome/i.test(userAgent) &&  (/ipad/i.test(userAgent) || !/mobile/i.test(userAgent)))
        next();
    else
        res.sendfile('app/dist/forbidden.html');
};
