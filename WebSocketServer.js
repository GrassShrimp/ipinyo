var FirebaseTokenGenerator = require("firebase-token-generator"),
    tokenGenerator = new FirebaseTokenGenerator('LvYNdREitnG0dmcqpSlpB74k33KGWq5KLHetoZZo'),
    nodejs_token = tokenGenerator.createToken({admin: "grassshrimp", system: "binyu"},{expires: Date.now()/1000 + 31557600}),
    browser_token = tokenGenerator.createToken({system: "binyu"},{expires: Date.now()/1000 + 31557600}),
    Firebase = require('firebase'),
    nodemailer = require("nodemailer"),
    OnlineUserRef = new Firebase('https://grassshrimp.firebaseio.com/OnlineUser'),
    TeamsRef = new Firebase('https://grassshrimp.firebaseio.com/Teams'),
    TopicsRef = new Firebase('https://grassshrimp.firebaseio.com/Topics'),
    FiltersRef = new Firebase('https://grassshrimp.firebaseio.com/Filters'),
    EventsRef = new Firebase('https://grassshrimp.firebaseio.com/Events'),
    PrivateTeamsRef = new Firebase('https://grassshrimp.firebaseio.com/PrivateTeams');

var smtp_config = require('./config/smtp.js');

var iceServers = [
    {
        url: "stun: stun3.l.google.com:19302"
    },
    {
        url: "turn: numb.viagenie.ca:3478?transport=udp",
        username: "duo342%40hotmail.com",
        credential: "cheh0891"
    }
];

(function deleteEvent(){
    EventsRef.auth(nodejs_token, function(){
        EventsRef.once('value', function(EventsSnapshot){
            EventsSnapshot.forEach(function(EventsCategorySnapshot){
                EventsCategorySnapshot.forEach(function(EventSnapshot){
                    var event_id = EventSnapshot.name(), event = EventSnapshot.val();
                    var length = event.events.length;
                    event.events.forEach(function(event_item,index){
                        var final = new Date(event_item.date.final);
                        if(Date.now() > final.valueOf()){
                            EventSnapshot.child('events').child(index).ref().remove(function(){
                                if(!--length){
                                    EventSnapshot.ref().remove();
                                }
                            });
                        }
                    });
                });
            });
        });
    });

    setTimeout(deleteEvent, 86400000);
}());

module.exports.listen = function (HttpServer) {
    var env = require('./env_config'),
        redis = require("redis"),
        RedisStore = require('socket.io/lib/stores/redis'),
        pub = redis.createClient(env.redis.port,env.redis.host),
        sub = redis.createClient(env.redis.port,env.redis.host),
        client = redis.createClient(env.redis.port,env.redis.host),
        subscriber = redis.createClient(env.redis.port,env.redis.host),
        _ = require('underscore'),
        io = require('socket.io').listen(HttpServer);

    client.on("error", function (err) {
        console.log("Error " + err);
    });

    io.set('store', new RedisStore({
        redis: redis,
        redisPub: pub,
        redisSub: sub,
        redisClient: client
    }));

    io.configure('production', function(){
        io.enable('browser client etag');
        io.set('log level', 1);

        io.set('transports', [
            'websocket'
        ]);
    });

    io.configure('development', function(){
        io.set('transports', ['websocket']);
    });

    io.configure(function (){
      io.set('authorization', function (handshakeData, callback) {
        if(handshakeData.xdomain) 
            callback('xdomain');
        else
            callback(null, true);
      });
    });

    subscriber.on("message", function(channel, userId){
        var socket = _(_(io.sockets.sockets).values()).findWhere({'userId': userId});

        if(!socket) return;

        socket.disconnect();
    });

    subscriber.subscribe("KickUser");

    io.sockets.on('connection', function (socket) {

        socket.on('getToken', function(){
            socket.emit('token', browser_token);
        });

        socket.on('ready',function(user){

            socket.userId = user.id;
            socket.gender = user.gender;
            
            socket.join('Index');
            if(_.isUndefined(socket.topics)) socket.topics = {};
            var interests = null,
                location = null,
                bio = null;
            if(user.interests) {
                interests = user.interests.data;
                TopicsRef.auth(nodejs_token, function(error, result){
                    interests.forEach(function(interest){
                        var category = interest.category.replace(/[\.\/\$#\[\]]/g,function(match,position,origin){return '0x'+match.charCodeAt(0)});
                        var interest_name = interest.name.replace(/[\.\/\$#\[\]]/g,function(match,position,origin){return '0x'+match.charCodeAt(0)});
                        TopicsRef.child(category).child(interest_name).child(user.id).set(user.gender, function(error){
                            if(_.isUndefined(socket.topics[category])) socket.topics[category] = [];
                            if(socket.topics[category].indexOf(interest_name) >= 0) return;
                            socket.topics[category].push(interest_name);
                        });
                    })
                });
            }

            if(user.groups){
                TopicsRef.auth(nodejs_token, function(error, result){
                    user.groups.data.forEach(function(group){
                        if(/open/i.test(group.privacy)){
                            var group_name = group.name.replace(/[\.\/\$#\[\]]/g,function(match,position,origin){return '0x'+match.charCodeAt(0)});
                            
                            TopicsRef.child('Groups').child(group_name).child(user.id).set(user.gender, function(error){
                                if(_.isUndefined(socket.topics.Groups)) socket.topics.Groups = [];

                                if(socket.topics.Groups.indexOf(group_name) >= 0) return;

                                socket.topics.Groups.push(group_name);
                            });
                        }
                    });
                });
            }

            if(user.location) {
                location = user.location.name;
            }

            if(user.bio) {
                bio = user.bio;
            }

            OnlineUserRef.auth(nodejs_token, function(error, result){
                if(error){
                    console.log(error);
                }
                else{
                    OnlineUserRef.child(user.id).set({name: user.name,picture: user.picture.data.url, gender: user.gender, location: location, bio: bio, interests: _.pluck(interests, 'name'), status: 'Online'}, function(error){
                        if(error){
                            console.log(error);
                        }
                    });
                }
            });
        });

        socket.on('createTeam', function(topics,private){
            var teamId = Date.now();

            if(private){
                PrivateTeamsRef.auth(nodejs_token, function(error,result){
                    PrivateTeamsRef.child(teamId).set({members: eval('({'+socket.userId+':socket.gender})')}, function(error){
                        if(error){
                            console.log(err);
                        }
                        else {
                            PrivateTeamsRef.child(teamId).once('value', function(PrivateTeamSnapshot){
                                if(!PrivateTeamSnapshot.val()) return;
                                socket.leave('Index');
                                socket.emit('reciveSingle','redirect',teamId,{type: 'success',title: 'WelCome',message: 'welcome this team'});
                            });
                        }
                    });
                });
            }
            else {
                var _topics = {};

                _(topics).each( function( list, key ) {
                    key = key.replace(/[\.\/\$#\[\]]/g,function(match,position,origin){return '0x'+match.charCodeAt(0)});

                    _topics[key] = list;
                });

                FiltersRef.auth(nodejs_token, function(error, result){
                    if(error){
                        console.log(error);
                    }
                    else {
                        _(_topics).each( function( list, key ) {
                            if(/^age$/i.test(key)) return;

                            list.forEach(function(item){
                                item = item.replace(/[\.\/\$#\[\]]/g,function(match,position,origin){return '0x'+match.charCodeAt(0)});
                                
                                FiltersRef.child(key).child(item).child(teamId).set('none');
                            });
                        });
                    }
                });

                TeamsRef.auth(nodejs_token, function(error,result){
                    TeamsRef.child(teamId).set({topics: _topics, members: eval('({'+socket.userId+':socket.gender})')}, function(error){
                        if(error){
                            console.log(err);
                        }
                        else {
                            TeamsRef.child(teamId).once('value', function(TeamSnapshot){
                                if(!TeamSnapshot.val()) return;
                                socket.leave('Index');
                                socket.emit('reciveSingle','redirect',teamId,{type: 'success',title: 'WelCome',message: 'welcome this team'});
                            });
                        }
                    });
                });
            }
        });

        socket.on('leaveTeam', function(teamId){
            if(teamId){
                var userId = socket.userId;

                if(!userId) return;

                TeamsRef.auth(nodejs_token, function(error, result){
                    TeamsRef.child(teamId).once('value', function(TeamSnapshot){
                        if(!TeamSnapshot.val()) return;

                        TeamSnapshot.child('members').child(userId).ref().remove(function(error){
                            if(error){
                                console.log(error);
                            }
                            else {
                                var area = socket.topics.Location;
                                if(area){
                                    TeamSnapshot.child('topics/Location').child(area).child(userId).ref().remove(function(){
                                        var member_area = _(TeamSnapshot.child('topics/Location').child(area).val()).omit(userId);

                                        if(_(member_area).isEmpty()) {
                                            FiltersRef.auth(nodejs_token, function(error, result){
                                                if(error){
                                                    console.log(error);
                                                }
                                                else {
                                                    FiltersRef.child('Location').child(area).child(teamId).ref().remove();
                                                }
                                            });
                                        }
                                        delete socket.topics.Location;
                                    });
                                }
                                
                                io.sockets.in(teamId).emit('reciveSingle','removeRTC',userId);
                                socket.leave(teamId);
                                
                                var members = _(TeamSnapshot.child('members').val()).omit(userId);

                                if(_(members).isEmpty()) {
                                    var topics = _(TeamSnapshot.child('topics').val()).omit('Age','Location');

                                    FiltersRef.auth(nodejs_token, function(){
                                        _(topics).each( function( list, key ) {
                                            list.forEach(function(item){
                                                item = item.replace(/[\.\/\$#\[\]]/g,function(match,position,origin){return '0x'+match.charCodeAt(0)});
                                                
                                                FiltersRef.child(key).child(item).child(teamId).ref().remove();
                                            });
                                        });
                                    });

                                    TeamSnapshot.ref().remove();
                                }
                            }
                        });
                    });
                });

                PrivateTeamsRef.auth(nodejs_token, function(error, result){
                    PrivateTeamsRef.child(teamId).once('value', function(PrivateTeamSnapshot){
                        if(!PrivateTeamSnapshot.val()) return;

                        PrivateTeamSnapshot.child('members').child(userId).ref().remove(function(error){
                            if(error){
                                console.log(error);
                            }
                            else {
                                io.sockets.in(teamId).emit('reciveSingle','removeRTC',userId);
                                socket.leave(teamId);
                            }
                        });
                    });
                });
            }
            else {
                socket.leave('Index');
            }
        });

        socket.on('InviteFriend', function(friendId){
            var sockets = _.values(io.sockets.sockets);

            var friend_socket = _.findWhere(sockets,{userId: friendId});

            friend_socket && friend_socket.emit('reciveSingle', 'BeInvited', socket.userId);
        });

        socket.on('RTCready', function(teamId,user){
            socket.join(teamId);
            socket.userId = user.id;
            socket.gender = user.gender;
            var interests = null,
                location = null,
                bio = null;
            if(_.isUndefined(socket.topics)) socket.topics = {};

            if(user.interests) {
                interests = user.interests.data;

                TopicsRef.auth(nodejs_token, function(error, result){
                    interests.forEach(function(interest){
                        var category = interest.category.replace(/[\.\/\$#\[\]]/g,function(match,position,origin){return '0x'+match.charCodeAt(0)});
                        var interest_name = interest.name.replace(/[\.\/\$#\[\]]/g,function(match,position,origin){return '0x'+match.charCodeAt(0)});
                        TopicsRef.child(category).child(interest_name).child(user.id).set(user.gender, function(error){
                            if(_.isUndefined(socket.topics[category])) socket.topics[category] = [];
                            if(socket.topics[category].indexOf(interest_name) >= 0) return;
                            socket.topics[category].push(interest_name);
                        });
                    })
                });
            }

            if(user.groups){
                TopicsRef.auth(nodejs_token, function(error, result){
                    user.groups.data.forEach(function(group){
                        if(/open/i.test(group.privacy)){
                            var group_name = group.name.replace(/[\.\/\$#\[\]]/g,function(match,position,origin){return '0x'+match.charCodeAt(0)});
                            
                            TopicsRef.child('Groups').child(group_name).child(user.id).set(user.gender, function(error){
                                if(_.isUndefined(socket.topics.Groups)) socket.topics.Groups = [];

                                if(socket.topics.Groups.indexOf(group_name) >= 0) return;

                                socket.topics.Groups.push(group_name);
                            });
                        }
                    });
                });
            }

            if(user.location) {
                location = user.location.name;
            }

            if(user.bio) {
                bio = user.bio;
            }

            TeamsRef.auth(nodejs_token, function(error, result){
                TeamsRef.child(teamId).once('value', function(TeamSnapshot){
                    if(!TeamSnapshot.val()) return;
                    
                    if(location){
                        FiltersRef.auth(nodejs_token, function(error, result){
                            FiltersRef.child('Location').child(location).child(teamId).set('none');
                        });

                        TeamsRef.child(teamId).child('topics/Location').child(location).child(user.id).set(user.gender, function(){
                            socket.topics.Location = location;
                        });
                    }

                    TeamsRef.child(teamId).child('members').child(user.id).set(user.gender, function(error){
                        if(error){
                            console.log(error);
                        }
                        else {
                            OnlineUserRef.auth(nodejs_token, function(error, result){
                                if(error){
                                    console.log(error);
                                }
                                else{
                                    OnlineUserRef.child(user.id).set({name: user.name, picture: user.picture.data.url, gender: user.gender, location: location, bio: bio, interests: _.pluck(interests, 'name'), status: teamId}, function(error){
                                        if(error){
                                            console.log(error);
                                        }
                                        else {
                                            socket.in(teamId).broadcast.emit('reciveSingle','newRTC', [user.id], 0, iceServers);

                                            var otherIds = _.without(_.pluck(io.sockets.clients(teamId),'userId'), user.id);

                                            if(!_.isEmpty(_.compact(otherIds))){
                                                socket.emit('reciveSingle','newRTC',otherIds, 1, iceServers);
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    });
                });
            });
            
            PrivateTeamsRef.auth(nodejs_token, function(error, result){
                PrivateTeamsRef.child(teamId).once('value', function(PrivateTeamSnapshot){
                    if(!PrivateTeamSnapshot.val()) return;

                    PrivateTeamsRef.child(teamId).child('members').child(user.id).set(user.gender, function(error){
                        if(error){
                            console.log(error);
                        }
                        else {
                            OnlineUserRef.auth(nodejs_token, function(error, result){
                                if(error){
                                    console.log(error);
                                }
                                else{
                                    OnlineUserRef.child(user.id).set({name: user.name, picture: user.picture.data.url, gender: user.gender, location: location, bio: bio, interests: _.pluck(interests, 'name'), status: teamId}, function(error){
                                        if(error){
                                            console.log(error);
                                        }
                                        else {
                                            socket.in(teamId).broadcast.emit('reciveSingle','newRTC', [user.id], 0, iceServers);

                                            var otherIds = _.without(_.pluck(io.sockets.clients(teamId),'userId'), user.id);

                                            if(!_.isEmpty(_.compact(otherIds))){
                                                socket.emit('reciveSingle','newRTC',otherIds, 1, iceServers);
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    });
                });
            });
        });

        socket.on('sendIceCandidate',function(to, candidate){
            var teamId = _.last(_.keys(io.sockets.manager.roomClients[socket.id])).match(/\d*$/)[0];
            
            var target = _.findWhere(io.sockets.clients(teamId),{userId: to});
            
            if(target)
                target.emit('reciveSingle','receiveIceCandidate',socket.userId,candidate);
        });

        socket.on('sendOffer',function(to, offer){
            var teamId = _.last(_.keys(io.sockets.manager.roomClients[socket.id])).match(/\d*$/)[0];
            
            var target = _.findWhere(io.sockets.clients(teamId),{userId: to});
            
            if(target)
                target.emit('reciveSingle','receiveOffer',socket.userId,offer);
        });

        socket.on('sendAnswer',function(to, answer){
            var teamId = _.last(_.keys(io.sockets.manager.roomClients[socket.id])).match(/\d*$/)[0];
            
            var target = _.findWhere(io.sockets.clients(teamId),{userId: to});
            
            if(target)
                target.emit('reciveSingle','receiveAnswer',socket.userId,answer);
        });

        socket.on('message', function(to, type, message, timestamp){
            var teamId = _.last(_.keys(io.sockets.manager.roomClients[socket.id])).match(/\d*$/)[0];
            
            var target = _.findWhere(io.sockets.clients(teamId),{userId: to});

            if(target) target.emit('reciveSingle','receiveMessage', socket.userId, type, message, timestamp);
        });

        socket.on('Stage', function(to, type, data, timestamp){
            var teamId = _.last(_.keys(io.sockets.manager.roomClients[socket.id])).match(/\d*$/)[0];

            var target = _.findWhere(io.sockets.clients(teamId),{userId: to});

            if(target) target.emit('reciveSingle','receiveStage', socket.userId, type, data, timestamp);
        });

        socket.on('single', function(to, type, currentTime, timestamp){
            var teamId = _.last(_.keys(io.sockets.manager.roomClients[socket.id])).match(/\d*$/)[0];

            var target = _.findWhere(io.sockets.clients(teamId),{userId: to});

            if(target) target.emit('reciveSingle','receiveSingle', socket.userId, type, currentTime, timestamp);
        });

        socket.on('State', function(to, type, data, timestamp){
            var teamId = _.last(_.keys(io.sockets.manager.roomClients[socket.id])).match(/\d*$/)[0];
            
            var target = _.findWhere(io.sockets.clients(teamId),{userId: to});

            if(target) target.emit('reciveSingle','receiveState', socket.userId, type, data, timestamp);
        });

        socket.on('sendMail', function(email, name, message){
            var transport = nodemailer.createTransport("SMTP", smtp_config);

            var mailOptions = {
                from: name+"<"+email+">",
                to: "service@ipinyo.com",
                generateTextFromHTML: true,
                subject: '我還有不懂的地方',
                html: '<h4>'+name+':</h4><p style="text-indent: 2em;">'+message+'</p>',
                text: ''
            }

            transport.sendMail(mailOptions,function(error, responseStatus){
                if(!error){
                    socket.emit('reciveSingle', 'alert', {type: 'success',title: '感謝您的回報',message: '我們會盡快回覆您'})
                }
            });

            transport.close();
        });

        socket.on('announcement', function(message){
            var teamId = _.last(_.keys(io.sockets.manager.roomClients[socket.id])).match(/\d*$/)[0];

            io.sockets.in(teamId).emit('reciveSingle', 'announcement', message);
        });

        socket.on('disconnect',function(){
            var userId = socket.userId;
            var teamId = _.last(_.keys(io.sockets.manager.roomClients[socket.id])).match(/\w*$/)[0];
            var topics = _(socket.topics).omit('Location');
            var area = (socket.topics)?socket.topics.Location:null;

            if(!userId) return;
            TopicsRef.auth(nodejs_token, function(){
                _(topics).each( function( list, key ) {
                    list.forEach(function(item){
                        TopicsRef.child(key).child(item).child(userId).ref().remove();
                    });
                });
            });

            OnlineUserRef.auth(nodejs_token, function(error, result){
                OnlineUserRef.child(userId).remove();
            });
            
            if(!teamId) {
                var over = false;

                TeamsRef.auth(nodejs_token, function(error, result){
                    TeamsRef.once('value', function(TeamsSnapshot){
                        
                        TeamsSnapshot.forEach(function(TeamSnapshot){
                            if(over || !TeamSnapshot.child('members').hasChild(userId)) return;

                            TeamSnapshot.child('members').child(userId).ref().remove(function(error){
                                if(error){
                                    console.log(error)
                                }
                                else{
                                    over = true;
                                    var teamId = TeamSnapshot.name();
                                    if(area){
                                        TeamSnapshot.child('topics/Location').child(area).child(userId).ref().remove(function(){
                                            var member_area = _(TeamSnapshot.child('topics/Location').child(area).val()).omit(userId);

                                            if(_(member_area).isEmpty()) {
                                                FiltersRef.auth(nodejs_token, function(error, result){
                                                    if(error){
                                                        console.log(error);
                                                    }
                                                    else {
                                                        FiltersRef.child('Location').child(area).child(teamId).ref().remove();
                                                    }
                                                });
                                            }
                                            delete socket.topics.Location;
                                        });
                                    }

                                    io.sockets.in(teamId).emit('reciveSingle','removeRTC',userId);

                                    var members = _(TeamSnapshot.child('members').val()).omit(userId);

                                    if(_(members).isEmpty()) {
                                        var topics = _(TeamSnapshot.child('topics').val()).omit('Age','Location');

                                        FiltersRef.auth(nodejs_token, function(){
                                            _(topics).each( function( list, key ) {
                                                list.forEach(function(item){
                                                    item = item.replace(/[\.\/\$#\[\]]/g,function(match,position,origin){return '0x'+match.charCodeAt(0)});
                                                    
                                                    FiltersRef.child(key).child(item).child(teamId).ref().remove();
                                                });
                                            });
                                        });

                                        TeamSnapshot.ref().remove();
                                    }
                                }
                            });
                        })
                    })
                });

                PrivateTeamsRef.auth(nodejs_token, function(error, result){

                    PrivateTeamsRef.once('value', function(PrivateTeamsSnapshot){

                        PrivateTeamsSnapshot.forEach(function(PrivateTeamSnapshot){

                            if(over || !PrivateTeamSnapshot.child('members').hasChild(userId)) return;

                            PrivateTeamSnapshot.child('members').child(userId).ref().remove(function(error){
                                if(error){
                                    console.log(error);
                                }
                                else {
                                    over = true;

                                    io.sockets.in(teamId).emit('reciveSingle','removeRTC',userId);
                                }
                            });
                        })
                    });
                });

                return;
            };

            TeamsRef.auth(nodejs_token, function(error, result){
                TeamsRef.child(teamId).once('value', function(TeamSnapshot){
                    if(!TeamSnapshot.val()) return;

                    TeamSnapshot.child('members').child(userId).ref().remove(function(error){
                        if(error){
                            console.log(error)
                        }
                        else{
                            if(area){
                                TeamSnapshot.child('topics/Location').child(area).child(userId).ref().remove(function(){
                                    var member_area = _(TeamSnapshot.child('topics/Location').child(area).val()).omit(userId);

                                    if(_(member_area).isEmpty()) {
                                        FiltersRef.auth(nodejs_token, function(error, result){
                                            if(error){
                                                console.log(error);
                                            }
                                            else {
                                                FiltersRef.child('Location').child(area).child(teamId).ref().remove();
                                            }
                                        });
                                    }
                                    delete socket.topics.Location;
                                });
                            }

                            io.sockets.in(teamId).emit('reciveSingle','removeRTC',userId);

                            var members = _(TeamSnapshot.child('members').val()).omit(userId);

                            if(_(members).isEmpty()) {
                                var topics = _(TeamSnapshot.child('topics').val()).omit('Age','Location');

                                FiltersRef.auth(nodejs_token, function(){
                                    _(topics).each( function( list, key ) {
                                        list.forEach(function(item){
                                            item = item.replace(/[\.\/\$#\[\]]/g,function(match,position,origin){return '0x'+match.charCodeAt(0)});
                                            
                                            FiltersRef.child(key).child(item).child(teamId).ref().remove();
                                        });
                                    });
                                });

                                TeamSnapshot.ref().remove();
                            }
                        }
                    });
                });
            });

            PrivateTeamsRef.auth(nodejs_token, function(error, result){
                PrivateTeamsRef.child(teamId).once('value', function(PrivateTeamSnapshot){
                    if(!PrivateTeamSnapshot.val()) return;
                    PrivateTeamSnapshot.child('members').child(userId).ref().remove(function(error){
                        if(error){
                            console.log(error);
                        }
                        else {
                            io.sockets.in(teamId).emit('reciveSingle','removeRTC',userId);
                        }
                    });
                });
            });
        });
    });
}