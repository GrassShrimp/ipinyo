angular.module('IPinYoApp', ['angularMoment']).config([
  '$routeProvider',
  function ($routeProvider) {
    $routeProvider.when('/', {
      templateUrl: 'dist/Index.html',
      controller: 'IndexCtrl',
      resolve: {
        me: ['Me',function (Me) {
          return Me('id', 'name', 'picture', 'gender', 'hometown', 'location', 'interests', 'groups.fields(privacy,name,id)','bio');
        }],
        stream: ['Stream',function (Stream) {
          return Stream();
        }],
        ready: ['$q','PassageWay','$rootScope', '$http', '$compile', function ($q, PassageWay, $rootScope, $http, $compile){
          var defer = $q.defer();

          if(!PassageWay.IsConnect()){
            $http({
              method: 'GET',
              url: 'dist/NoticeModal.html'
            }).success(function (data, status, headers, config) {
              var scope = $rootScope.$new(true);
              var NoticeModal = '';
              scope.close = function () {
                $(NoticeModal).modal('hide');
              };
              NoticeModal = $compile(data)(scope);
              if(_.isUndefined(angular.element('#NoticeModal')[0])) angular.element('body').append(NoticeModal);
              
              (function check_ready(){
                if(PassageWay.IsConnect()){
                  scope.close();
                  $rootScope.$broadcast('ready');
                  defer.resolve();
                  if (!$rootScope.$$phase) $rootScope.$apply();
                  return;
                }
                else {
                  setTimeout(check_ready,0);
                }
              }());

              $(NoticeModal).modal('show');
            });
          }
          else {
            defer.resolve();
          }
          return defer.promise;
        }]
      }
    }).when('/:teamId', {
      templateUrl: 'dist/Team.html',
      controller: 'TeamCtrl',
      resolve: {
        teamInfo: ['$q', '$route', '$rootScope', 'pubsub', 'FB', 'PrivateTeamsRef', 'TeamsRef', 'Me', 'TopicsRef', function ($q, $route, $rootScope, pubsub, FB, PrivateTeamsRef, TeamsRef, Me, TopicsRef) {
          var defer = $q.defer(), teamId = $route.current.params.teamId;
          
          async.parallel([
            function (callback) {
              PrivateTeamsRef.child(teamId).once('value', function (PrivateTeamSnapshot) {
                var PrivateTeam = PrivateTeamSnapshot.val();
                if (PrivateTeam) {
                  var members = _.keys(PrivateTeam.members);
                  if (_.isEmpty(members)) {
                    defer.reject({
                      type: 'danger',
                      title: '抱歉',
                      message: '此旅團已解散'
                    });
                    if (!$rootScope.$$phase)
                      $rootScope.$apply();
                  } else if (members.length < 4) {
                    Me('id').then(function (response) {
                      if (members.indexOf(response.id) >= 0) {
                        defer.resolve(PrivateTeam);
                        if (!$rootScope.$$phase)
                          $rootScope.$apply();
                        return;
                      }
                      FB.api('fql', { q: 'SELECT uid2 FROM friend WHERE uid1 IN (' + members.join(',') + ') AND uid2 = ' + response.id }, function (response) {
                        if (_.isEmpty(response.data)) {
                          defer.reject({
                            type: 'danger',
                            title: '抱歉',
                            message: '此旅團僅限好友加入'
                          });
                          if (!$rootScope.$$phase)
                            $rootScope.$apply();
                        } else {
                          defer.resolve(PrivateTeam);
                          if (!$rootScope.$$phase)
                            $rootScope.$apply();
                        }
                      });
                    });
                  } else {
                    defer.reject({
                      type: 'danger',
                      title: '抱歉',
                      message: '此旅團已額滿'
                    });
                    if (!$rootScope.$$phase)
                      $rootScope.$apply();
                  }
                } else {
                  callback(null, false);
                }
              });
            },
            function (callback) {
              async.waterfall([
                function (callback) {
                  var teams = angular.element('div.teams_content').scope() ? angular.element('div.teams_content').scope().teams : undefined;
                  if (teams)
                    callback(null, teams[teamId]);
                  else
                    callback(null, null);
                },
                function (team, callback) {
                  if (team)
                    callback(null, team);
                  TeamsRef.child(teamId).once('value', function (TeamSnapshot) {
                    var team = TeamSnapshot.val();
                    if (team)
                      callback(null, team);
                    else
                      callback(null, false);
                  });
                },
                function (team, callback) {
                  if (!team) {
                    callback(null, false);
                    return;
                  }
                  Me('birthday').then(function (me) {
                    var team_age = team.topics.Age;
                    if (!team_age) {
                      callback(null, team);
                      return;
                    }
                    var age = me.age, range = _.range(team_age.down, team_age.up + 1, 1);
                    if (range.indexOf(age) == -1) {
                      defer.reject({
                        type: 'danger',
                        title: '抱歉',
                        message: '您不符合此旅團需求的年齡'
                      });
                      if (!$rootScope.$$phase)
                        $rootScope.$apply();
                      return;
                    }
                    callback(null, team);
                  });
                },
                function (team, callback) {
                  if (!team) {
                    callback(null, false);
                    return;
                  }
                  if (!team.members) {
                    defer.reject({
                      type: 'danger',
                      title: '抱歉',
                      message: '此旅團已解散'
                    });
                    if (!$rootScope.$$phase)
                      $rootScope.$apply();
                  } else if (_.keys(team.members).length < 4) {
                    async.waterfall([
                      function (callback) {
                        Me('groups.fields(privacy,name,id)').then(function (response) {
                          if (_.isEmpty(response.groups)) {
                            callback(null, null);
                            return;
                          }
                          var groups = response.groups.data;
                          callback(null, _.pluck(groups, 'name'));
                        });
                      },
                      function (groups, callback) {
                        if (!groups) groups = [];
                        TopicsRef.child('Groups').once('value', function(GroupsSnapshot){
                          GroupsSnapshot.forEach(function(GroupSnapshot){
                            var group = GroupSnapshot.name().replace(/0x(\d{0,2})/g, function (match, submatch, position, origin) {
                              return String.fromCharCode(submatch);
                            });
                            if (groups.indexOf(group) >= 0) return;
                            groups.push(group);
                          });
                          callback(groups);
                        });
                      }
                    ], function (groups) {
                      if (team.topics && team.topics.Groups && _.isEmpty(_.intersection(team.topics.Groups, groups))) {
                        defer.reject({
                          type: 'danger',
                          title: '抱歉',
                          message: '您並非此旅團需求的社團成員'
                        });
                        if (!$rootScope.$$phase)
                          $rootScope.$apply();
                      } else {
                        defer.resolve(team);
                        if (!$rootScope.$$phase)
                          $rootScope.$apply();
                      }
                    });
                  } else {
                    defer.reject({
                      type: 'danger',
                      title: '抱歉',
                      message: '此旅團已額滿'
                    });
                    if (!$rootScope.$$phase)
                      $rootScope.$apply();
                  }
                }
              ], function (err, result) {
                callback(null, result);
              });
            }
          ], function (err, result) {
            defer.reject({
              type: 'danger',
              title: '抱歉',
              message: '此旅團並不存在'
            });
            if (!$rootScope.$$phase)
              $rootScope.$apply();
          });
          return defer.promise;
        }],
        me: ['Me',function (Me) {
          return Me('id', 'name', 'picture', 'gender', 'hometown', 'location', 'interests', 'groups.fields(privacy,name,id)','bio');
        }],
        stream: ['Stream',function (Stream) {
          return Stream();
        }],
        ready: ['$q','PassageWay','$rootScope','$http', '$compile', function ($q, PassageWay, $rootScope, $http, $compile){
          var defer = $q.defer();
          if(!PassageWay.IsConnect()){
            $http({
              method: 'GET',
              url: 'dist/NoticeModal.html'
            }).success(function (data, status, headers, config) {
              var scope = $rootScope.$new(true);
              var NoticeModal = '';
              scope.close = function () {
                $(NoticeModal).modal('hide');
              };
              NoticeModal = $compile(data)(scope);
              if(_.isUndefined(angular.element('#NoticeModal')[0])) angular.element('body').append(NoticeModal);
              
              (function check_ready(){
                if(PassageWay.IsConnect()){
                  scope.close();
                  $rootScope.$broadcast('ready');
                  defer.resolve();
                  if (!$rootScope.$$phase) $rootScope.$apply();
                  return;
                }
                else {
                  setTimeout(check_ready,0);
                }
              }());

              $(NoticeModal).modal('show');
            });
          }
          else {
            defer.resolve();
          }
          return defer.promise;
        }]
      }
    }).otherwise({ redirectTo: '/' });
  }
]).run(['PassageWay','pubsub','$rootScope','RTCcollection','$routeParams','$http','$location',function (PassageWay, pubsub, $rootScope, RTCcollection, $routeParams, $http, $location) {
  
  navigator.getUserMedia = navigator.getUserMedia || navigator.mozGetUserMedia || navigator.webkitGetUserMedia;
  $('div[ng-view]').css({ height: $(window).height() - $('footer').height() });
  moment.lang('zh-tw');
  PassageWay.connect();
  var loading = null;
  $http({
    method: 'GET',
    url: 'dist/floatingBarsG.html'
  }).success(function (data, status, headers, config) {
    loading = data;
  });
  $rootScope.$on('$routeChangeError', function (e, current, previous, rejection) {
    angular.element('.floatingBarsG').remove();
    $rootScope.$broadcast('alert', rejection);
    $location.path('/');
  });
  $rootScope.$on('$routeChangeStart', function (e, next, current) {
    $(window).unbind('resize');
    $('video#me').unbind('canplay');
    if(angular.element('.floatingBarsG')[0]) return;
    angular.element('body').append(loading);
  });
  $rootScope.$on('$routeChangeSuccess', function (e, next, current) {
    angular.element('.floatingBarsG').remove();
    if (current) {
      pubsub.clearChannels('message', 'single', 'State', 'Stage', 'newRTC', 'removeRTC');
      pubsub.publish('sendSingle', 'leaveTeam', current.pathParams.teamId);
    }
    RTCcollection.RemoveAll();
  });
  pubsub.subscribe('redirect', function (target, reason) {
    if (target != 'Index') {
      $location.path('/' + target);
      if (!$rootScope.$$phase)
        $rootScope.$apply();
    } else {
      $location.path('/');
      $rootScope.$broadcast('alert', reason);
      if (!$rootScope.$$phase)
        $rootScope.$apply();
    }
  });
}]).controller('AlertCtrl', ['$scope','FB','$location','pubsub',function ($scope, FB, $location,pubsub) {
  pubsub.subscribe('alert', function (information) {
    $scope.alert = information;
    $scope.display = true;
    if (!$scope.$$phase)
      $scope.$apply();
  });

  $scope.$on('alert', function (event, information) {
    $scope.alert = information;
    $scope.display = true;
    if (!$scope.$$phase)
      $scope.$apply();
  });
  $scope.close = function () {
    $scope.display = false;
    if (!$scope.$$phase)
      $scope.$apply();
  };
}]).controller('IdeaCtrl', ['$scope','$rootScope','$http',function ($scope, $rootScope, $http) {
  $http({
    method: 'GET',
    url: 'dist/IdeaModal.html'
  }).success(function (data, status, headers, config) {
    angular.element('body').append(data);
  });
}]).controller('InviteMsgCtrl', ['$scope', '$rootScope', 'pubsub', 'OnlineUserRef', 'TeamsRef', 'PrivateTeamsRef', '$location',function ($scope, $rootScope, pubsub, OnlineUserRef, TeamsRef, PrivateTeamsRef, $location) {
  $scope.Inviters = {};
  $scope.sayNo = function (inviter) {
    $scope.Inviters[inviter] = null;
    delete $scope.Inviters[inviter];
    if (!$scope.$$phase)
      $scope.$apply();
  };
  $scope.sayYes = function (inviter, teamId) {
    $scope.Inviters[inviter] = null;
    delete $scope.Inviters[inviter];
    $location.path('/' + teamId);
    angular.element('#InviteMsg').modal('hide');
    if (!$scope.$$phase)
      $scope.$apply();
  };
  angular.element('#InviteMsg').bind('hide.bs.modal', function () {
    $rootScope.$broadcast('readMsg');
  });
  pubsub.subscribe('BeInvited', function (inviter) {
    OnlineUserRef.child(inviter).once('value', function (OnlineUserSnapshot) {
      var user = OnlineUserSnapshot.val();
      PrivateTeamsRef.child(user.status).once('value', function (PrivateTeamSnapshot) {
        var team = PrivateTeamSnapshot.val();
        if (_.isEmpty(team))
          return;
        var img = new Image();
        img.src = user.picture;
        imagesLoaded(img, function () {
          $scope.Inviters[inviter] = {
            time: new Date(),
            user: user,
            team: team
          };
          $rootScope.$broadcast('GetMsg');
          if (!$scope.$$phase)
            $scope.$apply();
        });
      });
      TeamsRef.child(user.status).once('value', function (TeamSnapshot) {
        var team = TeamSnapshot.val();
        if (_.isEmpty(team))
          return;
        var img = new Image();
        img.src = user.picture;
        imagesLoaded(img, function () {
          if(team.topics.Location){
            team.topics.Location = _(team.topics.Location).keys();  
          }
          $scope.Inviters[inviter] = {
            time: new Date(),
            user: user,
            team: team
          };
          $rootScope.$broadcast('GetMsg');
          if (!$scope.$$phase)
            $scope.$apply();
        });
      });
    });
  });
}]).controller('OnlineUsersCtrl',['$scope', '$rootScope', 'OnlineUserRef','PassageWay', function ($scope, $rootScope, OnlineUserRef,PassageWay){
  
  $scope.peoples = 0;
  $scope.$on('ready', function(){

    OnlineUserRef.on('child_added', function(){
      $scope.peoples++;
      if (!$scope.$$phase) $scope.$apply();
    });

    OnlineUserRef.on('child_removed', function(){
      $scope.peoples--;
      if (!$scope.$$phase) $scope.$apply();
    });
  });
  
}]).controller('PrivacyPolicyCtrl',['$scope','$rootScope','$http', function ($scope,$rootScope,$http){
  $http({
    method: 'GET',
    url: 'dist/PrivacyPolicyModal.html'
  }).success(function (data, status, headers, config) {
    angular.element('body').append(data);
  });
}]).controller('ToursCtrl', ['$scope','$rootScope','$routeParams','$http','$compile','Me','pubsub',function ($scope,$rootScope,$routeParams,$http,$compile,Me,pubsub){
  if(!$.cookie('came')) {
    $.cookie('came',true);
    setTimeout(function(){
      $('a[ng-controller="ToursCtrl"]').popover({
        placement: 'top',
        title: '您第一次來嗎?<button type="button" class="close close_popover">&times;</button>',
        content: '讓我<span class="text-success">網站導遊</span>為您介紹這平台吧!',
        html: true
      }).one('shown.bs.popover', function(){
        var that = this;
        $('button.close_popover').one('click', function(){
          $(that).popover('destroy');
        });
      }).one('hide.bs.popover', function(){
        $(this).popover('destroy');
      }).popover('show');
    },3000);
  }

  var SendMailModal = '';
  $http({
    method: 'GET',
    url: 'dist/SendMailModal.html'
  }).success(function (data, status, headers, config) {
    $scope.close = function(){
      $scope.reportMessage = '';
      $(SendMailModal).modal('hide');
    };

    Me('email','name').then(function(response){
      if(response.error) {
        console.log(response.error);
        location.href = '/logout';
        return;
      }

      $scope.report = function(){
        pubsub.publish('sendSingle','sendMail',response.email,response.name,$scope.reportMessage);
        $scope.close();
      };
      if (!$scope.$$phase) $scope.$apply();
    });
    
    SendMailModal = $compile(data)($scope);
    angular.element('body').append(SendMailModal);
    if (!$scope.$$phase) $scope.$apply();
  });

  $scope.StartTours = function(){
    if($routeParams.teamId) {
      introJs().setOptions(
        {
          "skipLabel": "結束",
          "doneLabel": "結束",
          "nextLabel": "下一步",
          "prevLabel": "上一步",
          "showStepNumbers": false,
          "steps": [
            {
              element: document.querySelector('#me'),
              intro: '這裡是您的FB大頭照,<span class="text-success">點擊或把滑鼠移過去</span>的話,就會顯示<span class="text-success">您的相關資訊</span>,也是別人所看得到的資訊',
              position: 'right'
            },
            {
              element: document.querySelector('div.status'),
              intro: '這裡是您的狀態選單',
              position: 'top'
            },
            /*
            {
              element: document.querySelector('a.audio_toggle'),
              intro: '這裡可以讓您決定<span class="text-success">是否開啟麥克風</span>,點一下就能切換,預設是<span class="text-success">顯示目前狀態</span>',
              position: 'top'
            },
            {
              element: document.querySelector('a.video_toggle'),
              intro: '這裡可以讓您決定<span class="text-success">是否開啟視訊</span>,點一下就能切換,預設是<span class="text-success">顯示目前狀態</span>',
              position: 'bottom'
            },
            */
            {
              element: document.querySelector('a[href="#InviteMsg"]'),
              intro: '這裡是您的邀請函,若<span class="text-success">有朋友邀請這邀請函就會亮起來</span><i class="icon-envelope text-warning"></i>',
              position: 'top'
            },
            {
              element: document.querySelector('#InviteMsg div.modal-body'),
              intro: '這裡會顯示邀請函訊息,包括是<span class="text-success">哪位朋友邀請您的,以及該旅團的資訊</span>,點擊<button class="btn btn-success">好哇</button>,就能進入該旅團',
              position: 'bottom'
            },
            {
              element: document.querySelector('div.stage'),
              intro: '這裡是圖片與影片分享區,所<span class="text-success">分享的圖片跟影片都會在這邊顯示</span>,而且<span class="text-success">旅團內的人都看得到</span>',
              position: 'bottom'
            },
            {
              element: document.querySelector('div.stage i.clearStage'),
              intro: '點擊後可清理分享區',
              position: 'bottom'
            },
            {
              element: document.querySelector('div.chat'),
              intro: '這裡是文字聊天區',
              position: 'top'
            },
            {
              element: document.querySelector('div.users'),
              intro: '這旅團的所有人都會顯示在這裡以及他們的狀態',
              position: 'left'
            }
          ]
        }
      )
      .onchange(function(targetElement){
        if($('a[href="#InviteMsg"]').is(targetElement)) {
          $('#InviteMsg').modal('show');
        }
        else if($('div.stage').is(targetElement)) {
          $('#InviteMsg').modal('hide');
        }
      })
      .oncomplete(function(){
        $(SendMailModal).modal('show');
      })
      .start();
    }
    else {
      introJs().setOptions(
        {
          "skipLabel": "結束",
          "doneLabel": "結束",
          "nextLabel": "下一步",
          "prevLabel": "上一步",
          "showStepNumbers": false,
          "steps": [
            {
              element: document.querySelector('#TeamFilters'),
              intro: '在這裡您可在<span class="text-success">過濾出您想要的旅團</span>喔',
              position: 'right'
            },
            {
              element: document.querySelector('div[ng-controller="TeamsCtrl"]'),
              intro: '這是旅團列表,選擇其中一個旅團<span class="text-success">加入後就能與該旅團內的人進行交流</span>囉',
              position: 'left'
            },
            {
              element: document.querySelector('#me'),
              intro: '這裡是您的FB大頭照,<span class="text-success">點擊或把滑鼠移過去</span>的話,就會顯示<span class="text-success">您的相關資訊</span>,也是別人所看得到的資訊',
              position: 'left'
            },
            {
              element: document.querySelector('div.status'),
              intro: '這裡是您的狀態選單',
              position: 'top'
            },
            /*
            {
              element: document.querySelector('a.audio_toggle'),
              intro: '這裡可以讓您決定<span class="text-success">是否開啟麥克風</span>,點一下就能切換,預設是<span class="text-success">顯示目前狀態</span>',
              position: 'top'
            },
            {
              element: document.querySelector('a.video_toggle'),
              intro: '這裡可以讓您決定<span class="text-success">是否開啟視訊</span>,點一下就能切換,預設是<span class="text-success">顯示目前狀態</span>',
              position: 'bottom'
            },
            */
            {
              element: document.querySelector('a[href="#InviteMsg"]'),
              intro: '這裡是您的邀請函,點擊便能開啟,若<span class="text-success">有朋友邀請,這邀請函就會亮起來</span><i class="icon-envelope text-warning"></i>',
              position: 'left'
            },
            {
              element: document.querySelector('#InviteMsg div.modal-body'),
              intro: '這裡會顯示邀請函訊息,包括是<span class="text-success">哪位朋友邀請您的,以及該旅團的資訊</span>,點擊<button class="btn btn-success">好哇</button>,就能進入該旅團',
              position: 'bottom'
            },
            {
              element: document.querySelector('table[ng-controller="OnlineFriendsCtrl"]'),
              intro: '這裡是線上好友的列表,如果好友的狀態是<i class="icon-circle text-info link"></i>,則<span class="text-success">表示該好友在某一個旅團,您可以透過點選該好友</span>進入該旅團',
              position: 'left'
            },
            {
              element: document.querySelector('a[ng-controller="CreateTeamCtrl"]'),
              intro: '點選就會開啟建立旅團的表單',
              position: 'bottom'
            },
            {
              element: document.querySelector('#private'),
              intro: '這裡可以建立比較私人的旅團,選擇的話,<span class="text-success">您建立的旅團不會顯示在旅團列表上</span>,所以就只能透過在<span class="text-success">旅團內邀請的功能</span>或是<span class="text-success">朋友透過線上好友列表點選您</span>才能加入這旅團',
              position: 'right'
            },
            {
              element: document.querySelector('#team_topics'),
              intro: '這裡可以建立比較公開的旅團,選擇的話,<span class="text-success">您建立的旅團會顯示在旅團列表上</span>,所以好好選擇這旅團的主題是很重要的,<span class="text-success">一次最多只能選擇五個主題,加年齡是六個</span>',
              position: 'right'
            },
            {
              element: document.querySelector('button[ng-disabled="CanNotSubmit"]'),
              intro: '建立之後就會自動進入您新建立的旅團',
              position: 'top'
            }
          ]
        }
      )
      .onchange(function(targetElement){
        if($('a[href="#InviteMsg"]').is(targetElement)) {
          $('#InviteMsg').modal('show');
        }
        else if($('table.onlineusers').is(targetElement)) {
          $('#InviteMsg').modal('hide');
        }
        else if($('a[href="#CreateTeamModal"]').is(targetElement)) {
          $('#CreateTeamModal').modal('show');
        }
      })
      .onexit(function() {
        angular.element('#CreateTeamModal').scope().close();
        $('#InviteMsg').modal('hide');
      })
      .oncomplete(function(){
        angular.element('#CreateTeamModal').scope().close();
        $(SendMailModal).modal('show');
      })
      .start();
    }
  };
}]);;angular.module('IPinYoApp').controller('IndexCtrl', [
  'me',
  'stream',
  'pubsub',
  '$rootScope',
  function (me, stream, pubsub, $rootScope) {
    pubsub.publish('sendSingle', 'ready', me);
    var root = $('[ng-view]');
    
    $(window).bind('resize', function (e) {
      $('div[ng-view]').css({ height: $(window).height() - $('footer').height() });
      $('div.team_filter_container').css({ 'max-height': root.height() - $('div.team_filter_container').offset().top - 1 });
      $('div[ng-controller="TeamsCtrl"]').css({ 'max-height': root.height() - $('div[ng-controller="TeamsCtrl"]').offset().top - root.height() * 0.01 });
      $('.onlineusers').css({ 'max-height': root.height() - $('.onlineusers').offset().top });
      $('.onlineusers tbody').css({ 'max-height': root.height() - $('.onlineusers').offset().top - 20 });
    });
    $(window).trigger('resize');
  }
]).controller('TeamFilterCtrl', [
  '$scope',
  'arrayToggle',
  '$rootScope',
  'Me',
  'FiltersRef',
  'TopicsRef',
  function ($scope, arrayToggle, $rootScope, Me, FiltersRef, TopicsRef) {
    $scope.filters = {};
    $scope.selectedFilter = {};
    var groups = {};
    var me = '';
    Me('birthday','groups.fields(name)').then(function (response) {
      me = response;

      if (response.groups && response.groups.data){
        _(response.groups.data).pluck('name').forEach(function(name){
          if(_(groups).has(name)) return;

          groups[name] = false;
        });
      }
    });
    
    TopicsRef.child('Groups').on('child_added', function(GroupSnapshot){
      var group = GroupSnapshot.name().replace(/0x(\d{0,2})/g, function (match, submatch, position, origin) {
        return String.fromCharCode(submatch);
      });

      if(_(groups).has(group)) return;

      groups[group] = false;
    });

    TopicsRef.child('Groups').on('child_removed', function(GroupSnapshot){
      var group = GroupSnapshot.name().replace(/0x(\d{0,2})/g, function (match, submatch, position, origin) {
        return String.fromCharCode(submatch);
      });

      if(!_(groups).has(group)) return;

      delete groups[group];
    });

    FiltersRef.on('child_added', function(FilterSnapshot){
      var title = FilterSnapshot.name().replace(/0x(\d{0,2})/g, function (match, submatch, position, origin) {
        return String.fromCharCode(submatch);
      });
      
      FilterSnapshot.ref().on('child_added', function(FilterContentSnapshot){
        var title = FilterContentSnapshot.ref().parent().name().replace(/0x(\d{0,2})/g, function (match, submatch, position, origin) {
          return String.fromCharCode(submatch);
        });
        var filter = FilterContentSnapshot.name().replace(/0x(\d{0,2})/g, function (match, submatch, position, origin) {
          return String.fromCharCode(submatch);
        });

        if(/^groups$/i.test(title) && !_(groups).has(filter)) return;

        if(_($scope.filters[title]).isUndefined()) $scope.filters[title] = {};
        
        $scope.filters[title][filter] = false;
        if(!$scope.$$phase) $scope.$apply();
      });

      FilterSnapshot.ref().on('child_removed', function(FilterContentSnapshot){
        var title = FilterContentSnapshot.ref().parent().name().replace(/0x(\d{0,2})/g, function (match, submatch, position, origin) {
          return String.fromCharCode(submatch);
        });
        var filter = FilterContentSnapshot.name().replace(/0x(\d{0,2})/g, function (match, submatch, position, origin) {
          return String.fromCharCode(submatch);
        });

        if(_($scope.filters[title]).isUndefined() || !_($scope.filters[title]).has(filter)) return;

        if($scope.filters[title][filter]) {
          $scope.changeFilter(null,filter,title);
        }

        delete $scope.filters[title][filter];

        if(_(_($scope.filters[title]).omit('$$hashKey')).isEmpty()) delete $scope.filters[title];

        if(!$scope.$$phase) $scope.$apply();
      });

      (function(FilterRef){
        $scope.$on('$destroy', function(){
          FilterRef.off('child_added');
          FilterRef.off('child_removed');
        });
      }(FilterSnapshot.ref()));
    });

    FiltersRef.on('child_removed', function(FilterSnapshot){
      var title = FilterSnapshot.name().replace(/0x(\d{0,2})/g, function (match, submatch, position, origin) {
        return String.fromCharCode(submatch);
      });

      delete $scope.filters[title];

      if(!$scope.$$phase) $scope.$apply();
    });
    
    $scope.$on('$destroy', function(){
      FiltersRef.off('child_removed');
      FiltersRef.off('child_added');
      TopicsRef.child('Groups').off('child_added');
      TopicsRef.child('Groups').off('child_removed');
      EventsRef.off('child_removed');
      EventsRef.off('child_added');
    });

    $scope.filters.Age = [
      1,
      3,
      5,
      7,
      10
    ];
    
    $scope.changeFilter = function (event, item, type) {
      if (_.isUndefined($scope.selectedFilter[type]))
        $scope.selectedFilter[type] = [];
      if (/^age$/i.test(type)) {
        $(event.target).siblings('button').removeClass('active');
        var age = me.age;
        var down = age - item > 0 ? age - item : 0, up = age + item;
        var age_filter = {
            up: up,
            down: down
          };
        if ($scope.selectedFilter[type] && _.isEqual($scope.selectedFilter[type], age_filter))
          $scope.selectedFilter[type] = {};
        else
          $scope.selectedFilter[type] = age_filter;
      } else {
        $scope.filters[type][item] = arrayToggle($scope.selectedFilter[type], item);
      }
      if (_.isEmpty($scope.selectedFilter[type])) delete $scope.selectedFilter[type];
      $rootScope.$broadcast('changeFilter', $scope.selectedFilter);
      if(!$scope.$$phase) $scope.$apply();
    };
  }
]).controller('CreateTeamCtrl', [
  '$scope',
  '$http',
  '$compile',
  'arrayToggle',
  'pubsub',
  'Me',
  'TopicsRef',
  'EventsRef',
  function ($scope, $http, $compile, arrayToggle, pubsub, Me, TopicsRef, EventsRef) {
    var loading = null;
    $http({
      method: 'GET',
      url: 'dist/floatingBarsG.html'
    }).success(function (data, status, headers, config) {
      loading = data;
    });

    $http({
      method: 'GET',
      url: 'dist/CreateTeamModal.html'
    }).success(function (data, status, headers, config) {
      var CreateTeamModal = '';
      var me = '';
      Me('birthday').then(function (response) {
        me = response;
      });

      $scope.$on('$destroy', function () {
        $scope.close();
        $(CreateTeamModal).remove();
      });
      var selectedTopics = {};
      $scope.private = false;
      $scope.$watch('private', function () {
        $scope.CanNotSubmit = !$scope.private && _.isEmpty(_.flatten(_.values(selectedTopics)));
        if(!$scope.$$phase) $scope.$apply();
      });
      
      $scope.close = function () {
        $(CreateTeamModal).modal('hide').find('button').removeClass('active');
        $scope.CanNotSubmit = true;
        $scope.private = false;
        _(_(selectedTopics).omit('Age')).each( function( list, type ) {
          if(/^events$/i.test(type)){
            _($scope.topics[type]).each( function( value, category ) {
              _($scope.topics[type][category]).each( function( value, event_name ) {
                $scope.topics[type][category][event_name] = false;
              });
            });
            
            return;
          }

          list.forEach(function(item){
            $scope.topics[type][item] = false;
          });
        });
        selectedTopics = {};
        delete $scope.selectedTopics;
        $('div.panel-collapse.in','#team_topics').removeClass('in').addClass('collapse').css({
          height: 0
        });
      };
      
      $scope.create = function () {
        if ($scope.CanNotSubmit || !$scope.private && _(selectedTopics).isEmpty())
          return false;
        if ($scope.private) {
          selectedTopics = {};
        }
        pubsub.publish('sendSingle', 'createTeam', selectedTopics, $scope.private);
        $scope.CanNotSubmit = true;
        $scope.close();
        angular.element('body').append(loading);
      };
      $scope.addTopic = function (event, item, type, category) {
        if (_.isUndefined(selectedTopics[type]))
          selectedTopics[type] = [];
        if (/^age$/i.test(type)) {
          $(event.target).siblings('button').removeClass('active');
          var age = me.age;
          if(!_(item).isNumber() || item <= 0) return;
          var down = age - item > 0 ? age - item : 0, up = age + item;
          var age_topic = {
              up: up,
              down: down
            };
          if (selectedTopics[type] && _.isEqual(selectedTopics[type], age_topic))
            selectedTopics[type] = {};
          else
            selectedTopics[type] = age_topic;
        }
        else if(/^events$/i.test(type) && _($scope.topics).has(type) && _($scope.topics[type][category]).has(item)){
          $scope.topics[type][category][item] = arrayToggle(selectedTopics[type], item);
        }
        else if(_($scope.topics).has(type) && _($scope.topics[type]).has(item)) {
          $scope.topics[type][item] = arrayToggle(selectedTopics[type], item);
        }
        else {
          return;
        }
        if (_.isEmpty(selectedTopics[type])) delete selectedTopics[type];

        $scope.selectedTopics = _(selectedTopics).clone();
        $scope.CanNotSubmit = !$scope.private && _.isEmpty(_.flatten(_.values(selectedTopics)));
        if(!$scope.$$phase) $scope.$apply();
      };

      $scope.topics = {};

      $scope.topics.Age = [
        1,
        3,
        5,
        7,
        10
      ];

      EventsRef.on('child_added', function (EventCategorySnapshot) {
        (function(category,EventCategorySnapshot){
          if(_($scope.topics.Events).isUndefined()){
            $scope.topics.Events = {};
          }

          if(_($scope.topics.Events[category]).isUndefined()) $scope.topics.Events[category] = {};
          EventCategorySnapshot.ref().on('child_added', function(EventSnapshot){
            var event_id = EventSnapshot.name(), event_name = EventSnapshot.val().name;
            if($scope.topics.Events[category][event_name]) return;
            $scope.topics.Events[category][event_name] = false;
            if(!$scope.$$phase) $scope.$apply();
          });

          EventCategorySnapshot.ref().on('child_removed', function(EventSnapshot){
            var event_name = EventSnapshot.val().name;
            
            delete $scope.topics.Events[category][event_name];
            if(!$scope.$$phase) $scope.$apply();
          });

          $scope.$on('$destroy', function(){
            EventCategorySnapshot.ref().off('child_added');
            EventCategorySnapshot.ref().off('child_removed');
          });
        }(EventCategorySnapshot.name(),EventCategorySnapshot));
      });

      EventsRef.on('child_removed', function(EventCategorySnapshot){
        if(_($scope.topics.Events).isUndefined()){
          $scope.topics.Events = {};
        }
        var category = EventCategorySnapshot.name();

        delete $scope.topics.Events[category];
        if(!$scope.$$phase) $scope.$apply();
      });

      TopicsRef.on('child_added', function(TopicSnapshot){
        var title = TopicSnapshot.name().replace(/0x(\d{0,2})/g, function (match, submatch, position, origin) {
          return String.fromCharCode(submatch);
        });
        $scope.topics[title] = {};

        Me('groups.fields(privacy,name,id)').then(function (response) {
          if (!response.groups || !response.groups.data) return;
          
          var groups = response.groups.data;
          
          if (_.isEmpty($scope.topics.Groups)) {
            $scope.topics.Groups = [];
          }
          _.pluck(groups, 'name').forEach(function(name){

            if(_($scope.topics.Groups).has(name)) return;

            $scope.topics.Groups[name] = false;
          });

          if(!$scope.$$phase) $scope.$apply();
        });

        TopicSnapshot.ref().on('child_added', function(TopicContentSnapshot){
          var title = TopicContentSnapshot.ref().parent().name().replace(/0x(\d{0,2})/g, function (match, submatch, position, origin) {
            return String.fromCharCode(submatch);
          });
          var topic = TopicContentSnapshot.name().replace(/0x(\d{0,2})/g, function (match, submatch, position, origin) {
            return String.fromCharCode(submatch);
          });

          if(/^groups$/i.test(title) && _($scope.topics[title]).has(topic)) return;

          if(_($scope.topics[title]).isUndefined()) $scope.topics[title] = {};

          $scope.topics[title][topic] = false;

          if(!$scope.$$phase) $scope.$apply();
        });

        TopicSnapshot.ref().on('child_removed', function(TopicContentSnapshot){
          var title = TopicContentSnapshot.ref().parent().name().replace(/0x(\d{0,2})/g, function (match, submatch, position, origin) {
            return String.fromCharCode(submatch);
          });
          var topic = TopicContentSnapshot.name().replace(/0x(\d{0,2})/g, function (match, submatch, position, origin) {
            return String.fromCharCode(submatch);
          });
          
          if(_($scope.topics[title]).isUndefined() || !_($scope.topics[title]).has(topic)) return;

          if($scope.topics[title][topic]) {
            $scope.addTopic(null,topic,title);
          }

          delete $scope.topics[title][topic];

          if(_(_($scope.topics[title]).omit('$$hashKey')).isEmpty()) delete $scope.topics[title];

          if(!$scope.$$phase) $scope.$apply();
        });
        (function(TopicRef){
          $scope.$on('$destroy', function(){
            TopicRef.off('child_removed');
            TopicRef.off('child_added');
          });
        }(TopicSnapshot.ref()));
      });

      TopicsRef.on('child_removed', function (TopicSnapshot){
        var title = TopicSnapshot.name().replace(/0x(\d{0,2})/g, function (match, submatch, position, origin) {
          return String.fromCharCode(submatch);
        });

        if(_(_($scope.topics[title]).omit('$$hashKey')).isEmpty()) delete $scope.topics[title];

        if(!$scope.$$phase) $scope.$apply();
      });

      CreateTeamModal = $compile(data)($scope);
      angular.element('body').append(CreateTeamModal);

      $scope.$on('$destroy', function(){
        TopicsRef.off('child_removed');
        TopicsRef.off('child_added');
      });

    });
  }
]).controller('TeamsCtrl', [
  '$scope',
  'pubsub',
  'TeamsRef',
  'Me',
  'TopicsRef',
  function ($scope, pubsub, TeamsRef, Me, TopicsRef) {
    $scope.teams = {};
    Me('birthday','groups.fields(name)').then(function (response) {
      var me = response;
    
      var groups = [];

      if (response.groups && response.groups.data) groups = _(response.groups.data).pluck('name');

      TopicsRef.child('Groups').on('child_added', function(GroupSnapshot){
        var group = GroupSnapshot.name().replace(/0x(\d{0,2})/g, function (match, submatch, position, origin) {
          return String.fromCharCode(submatch);
        });

        if(groups.indexOf(group) >= 0) return;

        groups.push(group);
      });

      TopicsRef.child('Groups').on('child_removed', function(GroupSnapshot){
        var group = GroupSnapshot.name().replace(/0x(\d{0,2})/g, function (match, submatch, position, origin) {
          return String.fromCharCode(submatch);
        });

        var index = groups.indexOf(group);
        if(index < 0) return;
        groups.splice(index, 1);
      });

      TeamsRef.on('child_added', function (TeamSnapshot) {
        var teamId = TeamSnapshot.name(), team = TeamSnapshot.val();
        if (!team.topics)
          return;
        if ($scope.teams[teamId])
          return;
        var team_age = team.topics.Age;
        if (team_age) {
          var age = me.age;
          var range = _.range(team_age.down, team_age.up + 1, 1);
          if (range.indexOf(age) == -1)
            return;
        }
        var team_groups = team.topics.Groups;

        if(team_groups && _(_.intersection(team_groups,groups)).isEmpty()) return;

        var _topics = {};

        _(team.topics).each( function( list, key ) {
          key = key.replace(/0x(\d{0,2})/g, function (match, submatch, position, origin) {
            return String.fromCharCode(submatch);
          });

          if(/^location$/i.test(key)) {
            _topics[key] = _(team.topics[key]).keys();
          }
          else {
            _topics[key] = list;
          }
        });

        team.topics = _topics;

        $scope.teams[teamId] = team;
        if(!$scope.$$phase) $scope.$apply();
      });
      TeamsRef.on('child_changed', function (TeamSnapshot) {
        var teamId = TeamSnapshot.name(), team = TeamSnapshot.val();
        if (!$scope.teams[teamId])
          return;

        var _topics = {};

        _(team.topics).each( function( list, key ) {
          key = key.replace(/0x(\d{0,2})/g, function (match, submatch, position, origin) {
            return String.fromCharCode(submatch);
          });

          if(/^location$/i.test(key)) {
            _topics[key] = _(team.topics[key]).keys();
          }
          else {
            _topics[key] = list;
          }
        });

        team.topics = _topics;
        
        angular.extend($scope.teams[teamId], team);
        if(!$scope.$$phase) $scope.$apply();
      });
      TeamsRef.on('child_removed', function (TeamSnapshot) {
        var teamId = TeamSnapshot.name();
        if (!$scope.teams[teamId])
          return;
        $scope.teams[teamId] = null;
        delete $scope.teams[teamId];
        if(!$scope.$$phase) $scope.$apply();
      });
    });
    $scope.$on('changeFilter', function (e, Filter) {
      $scope.search = Filter;
      if(!$scope.$$phase) $scope.$apply();
    });

    $scope.$on('$destroy', function(){
      TeamsRef.off('child_removed');
      TeamsRef.off('child_changed');
      TeamsRef.off('child_added');
      TopicsRef.child('Groups').off('child_added');
      TopicsRef.child('Groups').off('child_removed');
    });
  }
]).controller('MeCtrl', [
  '$scope',
  'FB',
  function ($scope, FB) {
    $scope.$on('GetMsg', function (e) {
      window.invited = $scope.invited = true;
      if(!$scope.$$phase) $scope.$apply();
    });
    $scope.$on('readMsg', function (e) {
      window.invited = $scope.invited = false;
      if(!$scope.$$phase) $scope.$apply();
    });
    $scope.openInviteMsg = function () {
      angular.element('#InviteMsg').modal('show');
    };
    $scope.adjustAudio = function () {
      if (!window.stream)
        return;
      var audioTracks = window.stream.getAudioTracks()[0];
      if (!audioTracks)
        return;
      $scope.audio = audioTracks.enabled = !$scope.audio;
      if(!$scope.$$phase) $scope.$apply();
    };
    $scope.adjustVideo = function () {
      if (!window.stream)
        return;
      var videoTracks = window.stream.getVideoTracks()[0];
      if (!videoTracks)
        return;
      $scope.video = videoTracks.enabled = !$scope.video;

      var video = $('#me video');

      if($scope.video){
        video.attr({
          src: $scope.src
        });
      }
      else{
        video.attr({
          src: ''
        });
      }

      if(!$scope.$$phase) $scope.$apply();
    };
    $scope.displayIntro = function (e) {
      var intro = $(e.target).parent('div.user').children('div.intro');
      intro.css({ left: 0 - intro.width() });
    };

    FB.api('/me?fields=picture.width(263).height(197),interests.fields(name),bio', function (response) {
      if(response.error){
        console.log(response.error);
        location.href = '/logout';
        return;
      } 
      if (_.isEmpty(response.picture))
        return;
      var poster = response.picture.data.url;
      var img = new Image();
      img.src = poster;
      imagesLoaded(img, function () {
        $scope.poster = poster;
        $scope.src = window.URL.createObjectURL(window.stream);
        $scope.audio = (function () {
          if (!window.stream)
            return false;
          var audioTracks = window.stream.getAudioTracks()[0];
          if (!audioTracks)
            return false;
          return window.stream ? audioTracks.enabled : false;
        }());
        $scope.video = (function(){
          if (!window.stream)
            return false;
          var videoTracks = window.stream.getVideoTracks()[0];
          if (!videoTracks)
            return false;
          return window.stream ? videoTracks.enabled : false;
        }());
        $scope.invited = window.invited ? window.invited : false;
        $scope.Intro = {};
        if (response.interests && response.interests.data) {
          $scope.Intro.interests = _.pluck(response.interests.data, 'name');
        }
        if (response.bio) {
          $scope.Intro.bio = [response.bio];
        }
        if(!$scope.$$phase) $scope.$apply();
      });
    });
  }
]).controller('OnlineFriendsCtrl', [
  '$scope',
  'FB',
  '$location',
  'OnlineUserRef',
  function ($scope, FB, $location, OnlineUserRef) {
    $scope.onlineFriends = {};
    $scope.enter = function (teamId) {
      if (/^online$/i.test(teamId))
        return;
      $location.path('/' + teamId);
    };
    FB.api('fql', { q: 'SELECT uid2 FROM friend WHERE uid1 = me()' }, function (response) {
      if(response.error){
        console.log(response.error);
        location.href = '/logout';
        return;
      } 
      if (_.isEmpty(response.data))
        return;
      var fb_friends = _.pluck(response.data, 'uid2');
      var online_user_added = OnlineUserRef.on('child_added', function (OnlineUserSnapshot) {
        var userId = OnlineUserSnapshot.name(), user = OnlineUserSnapshot.val();
        if (fb_friends.indexOf(userId) < 0)
          return;
        var img = new Image();
        img.src = user.picture;
        imagesLoaded(img, function () {
          $scope.onlineFriends[userId] = user;
          if(!$scope.$$phase) $scope.$apply();
        });
      });
      var online_user_changed = OnlineUserRef.on('child_changed', function (OnlineUserSnapshot) {
        var userId = OnlineUserSnapshot.name(), user = OnlineUserSnapshot.val();
        if (fb_friends.indexOf(userId) < 0)
          return;
        user.picture = user.picture.replace(/0x(\d{0,2})/g, function (match, submatch, position, origin) {
          return String.fromCharCode(submatch);
        });
        var img = new Image();
        img.src = user.picture;
        imagesLoaded(img, function () {
          $scope.onlineFriends[userId] = user;
          if(!$scope.$$phase) $scope.$apply();
        });
      });
      var online_user_removed = OnlineUserRef.on('child_removed', function (OnlineUserSnapshot) {
        var userId = OnlineUserSnapshot.name(), user = OnlineUserSnapshot.val();
        if (fb_friends.indexOf(userId) < 0)
          return;
        $scope.onlineFriends[userId] = null;
        delete $scope.onlineFriends[userId];
        if(!$scope.$$phase) $scope.$apply();
      });

      $scope.$on('$destroy', function(){
        if(online_user_removed) OnlineUserRef.off('child_removed',online_user_removed);
        if(online_user_changed) OnlineUserRef.off('child_changed',online_user_changed);
        if(online_user_added) OnlineUserRef.off('child_added',online_user_added);
      });
    });
    if(!$scope.$$phase) $scope.$apply();
  }
]);;angular.module('IPinYoApp').controller('TeamCtrl', [
  'me',
  'stream',
  '$rootScope',
  'pubsub',
  'RTCcollection',
  '$routeParams',
  'teamInfo',
  function (me, stream, $rootScope, pubsub, RTCcollection, $routeParams, teamInfo) {
    pubsub.subscribe('newRTC', function (remotes, active, iceServers) {
      remotes.forEach(function (remote) {
        RTCcollection.CreateRTC(remote, active, iceServers);
      });
    });
    pubsub.subscribe('removeRTC', function (remote) {
      RTCcollection.RemoveRTC(remote);
      $rootScope.$broadcast('removeStream', remote);
    });
    $rootScope.$broadcast('getStream', stream);
    pubsub.publish('sendSingle', 'RTCready', $routeParams.teamId, me);
    var root = $('[ng-view]');

    $(window).bind('resize', function () {
      $('div[ng-view]').css({ height: $(window).height() - $('footer').height() });
      $('div.ShareContent').css({ 'max-height': root.height() * 0.99 - $('footer').height() });
      $('div.function').css({ 'height': root.height() - $('div.function').offset().top - $('a.leave').outerHeight() });
    });
    $(window).trigger('resize');
  }
]).controller('UserCtrl', [
  '$scope',
  'pubsub',
  'FB',
  '$rootScope',
  'OnlineUserRef',
  function ($scope, pubsub, FB, $rootScope, OnlineUserRef) {
    $scope.users = {};
    $scope.displayIntro = function (e) {
      var intro = $(e.target).parent('div.user').children('div.intro');
      intro.css({ left: 0 - intro.width() });
    };
    $scope.$on('addStream', function (e, id, stream) {
      FB.api('/' + id + '?fields=name,picture.height(157).width(188),gender', function (response) {
        if(response.error){ 
          console.log(response.error);
          location.href = '/logout';
          return;
        }

        if (_.isEmpty(response.picture))
          return;

        FB.api('fql', { q: 'SELECT uid2 FROM friend WHERE uid1 = me() AND uid2 = '+ id }, function (friend) {
          if(friend.error) {
            console.log(friend.error);
            location.href = '/logout';
            return;
          }

          (function () {
            var poster = response.picture.data.url;
            var img = new Image();
            img.src = poster;
            imagesLoaded(img, function () {
              $scope.users[id] = angular.extend({
                id: id,
                name: response.name,
                gender: response.gender,
                poster: poster,
                src: window.URL.createObjectURL(stream),
                audio: false,
                video: false,
                status: 'icon-ok',
                sharer: false,
                relation: _.isEmpty(friend.data)?"btn btn-primary icon-plus btn-xs":"icon-group",
                thumbsUpOrBefriend: _(friend.data).isEmpty()?
                  function(){
                    var that = this;
                    FB.ui({
                      method: 'friends',
                      id: this.id
                    }, function(response){
                      if(response && !response.action) return;

                      FB.api('fql', { q: 'SELECT uid2 FROM friend WHERE uid1 = me() AND uid2 = '+ id }, function (friend) {
                        if(friend.error) {
                          console.log(friend.error);
                          location.href = '/logout';
                          return;
                        }
                        that.thumbsUpOrBefriend = function(){return false;};
                        if(_(friend.data).isEmpty()){
                          pubsub.publish('sync', 'State', 'relation', "btn icon-plus btn-xs btn-warning", Date.now(), that.id);
                          return;
                        }
                        that.relation = "icon-group";
                        pubsub.publish('sync', 'State', 'relation', "icon-group", Date.now(),that.id);
                        if(!$scope.$$phase) $scope.$apply();
                      });
                    });
                  }:function(){ return false;}
              }, _.omit($scope.users[id],'src'));
              if(!$scope.$$phase) $scope.$apply();
            });
          }());
        });
      });

      OnlineUserRef.child(id).once('value', function(OnlineUserSnapshot){
        var user = OnlineUserSnapshot.val();

        if (!$scope.users[id])
          $scope.users[id] = {};
        $scope.users[id].Intro = _(user).pick('interests','bio','location');
        if(!$scope.$$phase) $scope.$apply();
      });
    });

    pubsub.subscribe('State', function (who, type, data) {
      if (!$scope.users[who])
        $scope.users[who] = {};
      $scope.users[who][type] = data;
      if(!$scope.$$phase) $scope.$apply();
      if(/^video$/i.test(type)) {
        var video = $('#'+$scope.users[who].id+' video');
        if(data){
          video.attr({
            src: $scope.users[who].src
          });
        }
        else{
          video.attr({
            src: ''
          });
        }
      }
      else if(/^status$/i.test(type) && _(_(_(_($scope.users).values()).pluck('status')).uniq()).isEqual(['icon-ok']) && angular.element('#me').scope().SyncState === 'icon-ok') {
        $rootScope.$broadcast('allReady');
      }
      if(!$scope.$$phase) $scope.$apply();
    });
    $scope.$on('Sharer', function (e, who) {
      if ($scope.users[who] && $scope.users[who].sharer)
        return;
      var before_sharer = _.findWhere($scope.users, { sharer: true });
      if (before_sharer)
        before_sharer.sharer = false;
      if ($scope.users[who])
        $scope.users[who].sharer = true;
      if(!$scope.$$phase) $scope.$apply();
    });
    $scope.$on('removeStream', function (e, id) {
      $scope.$apply(function () {
        $scope.users[id] = null;
        delete $scope.users[id];
      });
    });
  }
]).controller('UserMeCtrl', [
  '$scope',
  'pubsub',
  'FB',
  '$rootScope',
  function ($scope, pubsub, FB, $rootScope) {
    var origin_audio = null;
    $scope.SyncState = 'icon-ok';
    $scope.$on('GetMsg', function (e) {
      window.invited = $scope.invited = true;
      if(!$scope.$$phase) $scope.$apply();
    });
    $scope.$on('readMsg', function (e) {
      window.invited = $scope.invited = false;
      if(!$scope.$$phase) $scope.$apply();
    });
    $scope.adjustAudio = function () {
      if (!window.stream)
        return;
      var audioTracks = window.stream.getAudioTracks()[0];
      if (!audioTracks)
        return;
      origin_audio = audioTracks.enabled = $scope.audio = !$scope.audio;
      if(!$scope.$$phase) $scope.$apply();
    };
    $scope.adjustVideo = function () {
      if (!window.stream)
        return;
      var videoTracks = window.stream.getVideoTracks()[0];
      if (!videoTracks)
        return;
      videoTracks.enabled = $scope.video = !$scope.video;

      var video = $('#me video');

      if($scope.video){
        video.attr({
          src: $scope.src
        });
      }
      else{
        video.attr({
          src: ''
        });
      }

      if(!$scope.$$phase) $scope.$apply();
    };
    $scope.$watch('audio', function () {
      pubsub.publish('sync', 'State', 'audio', $scope.audio);
    });
    $scope.$watch('video', function(){
      pubsub.publish('sync', 'State', 'video', $scope.video);
    });
    $scope.displayIntro = function (e) {
      var intro = $(e.target).parent('div.user').children('div.intro');
      intro.css({ right: 0 - intro.width() });
    };
    $scope.$on('audio', function (e, value) {
      if (!window.stream)
        return;
      var audioTracks = window.stream.getAudioTracks()[0];
      if (!audioTracks)
        return;
      audioTracks.enabled = $scope.audio = value ? origin_audio : value;
      if(!$scope.$$phase) $scope.$apply();
    });
    
    $scope.$on('SyncState', function(e, value){
      $scope.SyncState = value;
      pubsub.publish('sync', 'State', 'status', value);
      var users = angular.element('div.users').scope().users;
      if((_(users).isEmpty() ||  _(_(_(_(users).values()).pluck('status')).uniq()).isEqual(['icon-ok'])) && $scope.SyncState === 'icon-ok') {
        $rootScope.$broadcast('allReady');
      }
      if(!$scope.$$phase) $scope.$apply();
    });
    FB.api('me?fields=picture.height(157).width(188),interests.fields(name),bio,location', function (response) {
      if(response.error){ 
        console.log(response.error);
        location.href = '/logout';
        return;
      }

      if (_.isEmpty(response.picture))
        return;
      var poster = response.picture.data.url;
      var img = new Image();
      img.src = poster;
      imagesLoaded(img, function () {
        $scope.poster = poster;
        $scope.src = window.URL.createObjectURL(window.stream);
        origin_audio = $scope.audio = (function () {
          if (!window.stream)
            return false;
          var audioTracks = window.stream.getAudioTracks()[0];
          if (!audioTracks)
            return false;
          return window.stream ? audioTracks.enabled : false;
        }());
        $scope.video = (function(){
          if (!window.stream)
            return false;
          var videoTracks = window.stream.getVideoTracks()[0];
          if (!videoTracks)
            return false;
          return window.stream ? videoTracks.enabled : false;
        }());
        $scope.invited = window.invited ? window.invited : false;
        $scope.Intro = {};
        if (response.interests && response.interests.data) {
          $scope.Intro.interests = _.pluck(response.interests.data, 'name');
        }
        if (response.bio) {
          $scope.Intro.bio = response.bio;
        }
        if(response.location) {
          $scope.Intro.location = response.location.name;
        }
        if(!$scope.$$phase) $scope.$apply();
      });
    });
  }
]).controller('ChatCtrl', [
  '$scope',
  'pubsub',
  'FB',
  function ($scope, pubsub, FB) {
    FB.api('/me?fields=id,name,gender', function (user) {
      if(user.error){ 
        console.log(user.error);
        location.href = '/logout';
        return;
      }

      var ChatRecord = angular.element('div.chatRecord'), reciveMessage = function (who, message) {
          message = '<div class="message">' + '<span class="chatname '+who.gender+'">' + who.name + '</span>' + ': ' + message + '</div>';
          ChatRecord.append(message);
          ChatRecord.scrollTop(ChatRecord[0].scrollHeight);
        }, sendMessage = function (message) {
          $scope.message = '';
          pubsub.publish('sync', 'message', 'message', message);
          reciveMessage(user, message);
        };
      pubsub.subscribe('message', function (who, type, message) {
        var user = angular.element($('div.users')).scope().users[who];
        reciveMessage(user, message);
      });
      pubsub.subscribe('announcement', function (message){
        reciveMessage({gender: 'system',name: '系統'}, message);
      });
      $scope.say = function (event, message) {
        if (event.type == 'keypress' && event.keyCode == 13) {
          sendMessage(message);
        } else if (event.type == 'click') {
          sendMessage(message);
        }
      };
    });
  }
]).controller('FunctionsCtrl', [
  '$scope',
  'pubsub',
  '$rootScope',
  function ($scope, pubsub, $rootScope) {
    $scope.$on('$destroy', function (e) {
      $('body').unbind('click');
    });
    $('body').bind('click', function (e) {
      if (!$(e.target).parents('div.ShareContent').length) {
        $scope.albums_click = '';
        $scope.videos_click = '';
        $scope.friends_click = '';
        $scope.Events_click = '';
        $scope.youtube_click = '';
        angular.element($('div.ShareContent')).scope().active = false;
        if(!$scope.$$phase) $scope.$apply();
      }
    });
    $scope.getAlbums = function (event) {
      event.stopPropagation();
      if (!$scope.albums_click) {
        $scope.albums_click = 'active';
        $scope.videos_click = '';
        $scope.friends_click = '';
        $scope.Events_click = '';
        $scope.youtube_click = '';
        $rootScope.$broadcast('GetFBAlbums');
      } else {
        angular.element($('div.ShareContent')).scope().active = false;
        $scope.albums_click = '';
      }
      if(!$scope.$$phase) $scope.$apply();
    };
    $scope.getVideos = function (event) {
      event.stopPropagation();
      if (!$scope.videos_click) {
        $scope.albums_click = '';
        $scope.videos_click = 'active';
        $scope.friends_click = '';
        $scope.Events_click = '';
        $scope.youtube_click = '';
        $rootScope.$broadcast('GetFBVideos');
      } else {
        angular.element($('div.ShareContent')).scope().active = false;
        $scope.videos_click = '';
      }
      if(!$scope.$$phase) $scope.$apply();
    };
    $scope.getFriends = function (event) {
      event.stopPropagation();
      if (!$scope.friends_click) {
        $scope.albums_click = '';
        $scope.videos_click = '';
        $scope.friends_click = 'active';
        $scope.Events_click = '';
        $scope.youtube_click = '';
        $rootScope.$broadcast('GetFBFriends');
      } else {
        angular.element($('div.ShareContent')).scope().active = false;
        $scope.friends_click = '';
      }
      if(!$scope.$$phase) $scope.$apply();
    };
    $scope.getYoutube = function(event) {
      event.stopPropagation();
      if (!$scope.youtube_click) {
        $scope.albums_click = '';
        $scope.videos_click = '';
        $scope.friends_click = '';
        $scope.Events_click = '';
        $scope.youtube_click = 'active';
        $rootScope.$broadcast('GetMyYoutube');
      } else {
        angular.element($('div.ShareContent')).scope().active = false;
        $scope.youtube_click = '';
      }
      if(!$scope.$$phase) $scope.$apply();
    };
    $scope.getEvents = function (event) {
      event.stopPropagation();
      if (!$scope.Events_click) {
        $scope.albums_click = '';
        $scope.videos_click = '';
        $scope.friends_click = '';
        $scope.Events_click = 'active';
        $scope.youtube_click = '';
        $rootScope.$broadcast('GetEvents');
      } else {
        angular.element($('div.ShareContent')).scope().active = false;
        $scope.Events_click = '';
      }
      if(!$scope.$$phase) $scope.$apply();
    };
  }
]).controller('FunctionContentCtrl', [
  '$scope',
  'pubsub',
  '$compile',
  '$http',
  '$rootScope',
  'OnlineUserRef',
  'EventsRef',
  function ($scope, pubsub, $compile, $http, $rootScope, OnlineUserRef, EventsRef) {
    var ShareContent = angular.element('div.ShareContent');
    $http({
      method: 'GET',
      url: 'dist/FBAlbums.html'
    }).success(function (data, status, headers, config) {
      $scope.albumTemplate = $compile(data)($scope);
    });
    $http({
      method: 'GET',
      url: 'dist/FBVideos.html'
    }).success(function (data, status, headers, config) {
      $scope.videoTemplate = $compile(data)($scope);
    });
    $http({
      method: 'GET',
      url: 'dist/FBPhotos.html'
    }).success(function (data, status, headers, config) {
      $scope.photoTemplate = $compile(data)($scope);
    });
    $http({
      method: 'GET',
      url: 'dist/facebookG.html'
    }).success(function (data, status, headers, config) {
      $scope.LoadingSnippet = data;
    });
    $http({
      method: 'GET',
      url: 'dist/FBFriends.html'
    }).success(function (data, status, headers, config) {
      $scope.friendTemplate = $compile(data)($scope);
    });
    $http({
      method: 'GET',
      url: 'dist/MyYoutube.html'
    }).success(function (data, status, headers, config) {
      $scope.myyoutubeTemplate = $compile(data)($scope);
    });
    $http({
      method: 'GET',
      url: 'dist/Events.html'
    }).success(function (data, status, headers, config) {
      $scope.eventTemplate = $compile(data)($scope);
    });
    var getPartialPhoto = function (album_id, photo_count, current) {
      var readMoreButton = angular.element('<button class="btn btn-block" data-complete-text="Read More">Read More</button>').bind('click', function (event) {
          $(this).button('loading');
          $(this).html($scope.LoadingSnippet);
          var that = this;
          FB.api('fql', { q: 'SELECT caption, src, src_big FROM photo WHERE album_object_id = ' + album_id + ' LIMIT ' + current + ',10' }, function (Photos) {
            if(Photos.error){ 
              console.log(Photos.error);
              location.href = '/logout';
              return;
            }

            if (_.isEmpty(Photos.data)) {
              $(that).button('complete');
              $(that).unbind('*').remove();
            }
            current += 10;
            var i = 0, length = Photos.data.length;
            Photos.data.forEach(function (photo) {
              var img = new Image();
              img.src = photo.src;
              imagesLoaded(img, function () {
                if (++i == length) {
                  $scope.Photos = $scope.Photos.concat(Photos.data);
                  $(that).button('complete');
                  if (current >= photo_count) {
                    $(that).unbind('*').remove();
                  }
                  if(!$scope.$$phase) $scope.$apply();
                }
              });
            });
          });
        });
      FB.api('fql', { q: 'SELECT caption, src, src_big FROM photo WHERE album_object_id = ' + album_id + ' LIMIT ' + current + ',10' }, function (Photos) {
        if(Photos.error){ 
          console.log(Photos.error);
          location.href = '/logout';
          return;
        }
        current += 10;
        var i = 0, length = Photos.data.length;
        Photos.data.forEach(function (photo) {
          var img = new Image();
          img.src = photo.src;
          imagesLoaded(img, function () {
            if (++i == length) {
              $scope.Photos = Photos.data;
              if (current >= photo_count) {
                angular.element('#' + album_id + ' ' + '.panel-body').html($scope.photoTemplate);
              } else {
                angular.element('#' + album_id + ' ' + '.panel-body').html($scope.photoTemplate).append(readMoreButton);
              }
              if(!$scope.$$phase) $scope.$apply();
            }
          });
        });
      });
    };
    $scope.getPhotos = function (event, album_id, photo_count) {
      event.preventDefault();
      if (!angular.element('#' + album_id).hasClass('in')) {
        angular.element('#' + album_id + ' ' + '.panel-body').html('<div class="progress progress-striped active"><div class="progress-bar"  role="progressbar" style="width: 100%;"></div></div>');
        getPartialPhoto(album_id, photo_count, 0);
      }
    };
    $scope.share = function (event, type, src) {
      event.preventDefault();
      $rootScope.$broadcast('share', type, src);
    };
    var invited = {};
    $scope.invited = {};
    $scope.InviteFriend = function (event,friendId) {
      event.preventDefault();
      event.stopImmediatePropagation();
      if (!invited[friendId] || Date.now() - invited[friendId] >= 60000) {
        $scope.invited[friendId] = invited[friendId] = Date.now();
        pubsub.publish('sendSingle', 'InviteFriend', friendId);
        setTimeout(function(){
          $scope.invited[friendId] = invited[friendId] = false;
          if(!$scope.$$phase) $scope.$apply();
        },60000);
        if(!$scope.$$phase) $scope.$apply();
      }
    };
    $scope.$on('GetFBAlbums', function () {
      $scope.active = true;
      ShareContent.html('<div class="progress progress-striped active"><div class="progress-bar"  role="progressbar" style="width: 100%;"></div></div>');
      FB.api('fql', { q: 'SELECT name, photo_count, object_id FROM album WHERE owner = me() and type != \'app\' and photo_count > 0' }, function (Ablums) {
        if(Ablums.error){ 
          console.log(Ablums.error);
          location.href = '/logout';
          return;
        }
        $scope.Ablums = Ablums.data;
        ShareContent.html($scope.albumTemplate);
        if(!$scope.$$phase) $scope.$apply();
      });
    });
    $scope.$on('GetFBVideos', function () {
      $scope.active = true;
      ShareContent.html('<div class="progress progress-striped active"><div class="progress-bar"  role="progressbar" style="width: 100%;"></div></div>');
      FB.api('fql', { q: 'SELECT title, description, src, src_hq, thumbnail_link FROM video WHERE owner = me()' }, function (Videos) {
        if(Videos.error){
          console.log(Videos.error);
          location.href = '/logout';
          return;
        } 

        var vidoes = Videos.data, mp4_videos = [];
        if (_.isEmpty(vidoes)) {
          $scope.Videos = [];
          ShareContent.html($scope.videoTemplate);
          if(!$scope.$$phase) $scope.$apply();
        }
        var i = 0, videos_count = vidoes.length;
        vidoes.forEach(function (video) {
          if (!/\.mp4/i.test(video.src) || !/\.mp4/i.test(video.src_hq)) {
            ++i;
            return;
          }
          var img = new Image();
          img.src = video.thumbnail_link;
          imagesLoaded(img, function () {
            mp4_videos.push(video);
            if (++i == videos_count) {
              $scope.Videos = mp4_videos;
              ShareContent.html($scope.videoTemplate);
              if(!$scope.$$phase) $scope.$apply();
            }
          });
        });
      });
    });
    $scope.$on('GetFBFriends', function () {
      $scope.active = true;
      ShareContent.html('<div class="progress progress-striped active"><div class="progress-bar"  role="progressbar" style="width: 100%;"></div></div>');
      FB.api('fql', { q: 'SELECT uid2 FROM friend WHERE uid1 = me()' }, function (response) {
        if(response.error){ 
          console.log(response.error);
          location.href = '/logout';
          return;
        }
        var fb_friends = _.pluck(response.data, 'uid2');
        $scope.onlineFriends = {};
        ShareContent.html($scope.friendTemplate);
        var online_user_added = OnlineUserRef.on('child_added', function (OnlineUserSnapshot) {
          var userId = OnlineUserSnapshot.name(), user = OnlineUserSnapshot.val();
          if (fb_friends.indexOf(userId) < 0)
            return;
          var img = new Image();
          img.src = user.picture;
          imagesLoaded(img, function () {
            $scope.onlineFriends[userId] = user;
            if(!$scope.$$phase) $scope.$apply();
          });
        });
        var online_user_removed = OnlineUserRef.on('child_removed', function (OnlineUserSnapshot) {
          var userId = OnlineUserSnapshot.name(), user = OnlineUserSnapshot.val();
          if (fb_friends.indexOf(userId) < 0)
            return;
          $scope.onlineFriends[userId] = null;
          delete $scope.onlineFriends[userId];
          if(!$scope.$$phase) $scope.$apply();
        });
        (function(online_user_removed,online_user_added){
          $scope.$on('$destroy', function(){
            if(online_user_removed) OnlineUserRef.off('child_removed',online_user_removed);
            if(online_user_added) OnlineUserRef.off('child_added',online_user_added);
          });
        }(online_user_removed,online_user_added));
      });
    });

    $scope.$on('GetMyYoutube', function(){
      $scope.active = true;
      $scope.login = true;
      $scope.youtube = {};
      var playlistId, nextPageToken;
      ShareContent.html('<div class="progress progress-striped active"><div class="progress-bar"  role="progressbar" style="width: 100%;"></div></div>');
      var readMoreButton = angular.element('<button class="btn btn-block" data-complete-text="Read More">Read More</button>');
      var createDisplayThumbnail = function(videoSnippet,count) {
        var thumbnailUrl = videoSnippet.thumbnails.medium.url;
        var img = new Image();
        img.src = thumbnailUrl;
        imagesLoaded(img, function () {
          if(_($scope.youtube).isEmpty()) {
            ShareContent.html($scope.myyoutubeTemplate);

            if(nextPageToken) {
              ShareContent.append(readMoreButton);
            }
          }
          $scope.youtube[videoSnippet.resourceId.videoId] = {
            title: videoSnippet.title,
            thumbnails: thumbnailUrl
          };
          if(!$scope.$$phase) $scope.$apply();

          if(!(--count)) readMoreButton.button('complete');
        });
      };

      var requestVideoPlaylist = function(playlistId, pageToken){
        var requestOptions = {
          playlistId: playlistId,
          part: 'snippet',
          maxResults: 10
        };
        if (pageToken) {
          requestOptions.pageToken = pageToken;
        }
        var request = gapi.client.youtube.playlistItems.list(requestOptions);
        request.execute(function(response) {
          nextPageToken = response.result.nextPageToken;

          if(!nextPageToken) {
            readMoreButton.unbind('*').remove();
          }
          
          var playlistItems = response.result.items;
          if (playlistItems) {
            $.each(playlistItems, function(index, item) {
              createDisplayThumbnail(item.snippet,playlistItems.length);
            });
          }
        });
      };

      readMoreButton.bind('click', function(){
        $(this).button('loading');
        $(this).html($scope.LoadingSnippet);
        requestVideoPlaylist(playlistId,nextPageToken);
      });

      var requestUserPlaylistId = function(){
        var request = gapi.client.youtube.channels.list({
          mine: true,
          part: 'contentDetails'
        });
        request.execute(function(response) {
          playlistId = response.result.items[0].contentDetails.relatedPlaylists.favorites;
          requestVideoPlaylist(playlistId);
        });
      };

      var handleAuthResult = function (authResult) {
        ShareContent.html('<div class="progress progress-striped active"><div class="progress-bar"  role="progressbar" style="width: 100%;"></div></div>');
        if (authResult) {
          $scope.login = true;
          
          gapi.client.load('youtube', 'v3', function() {
            requestUserPlaylistId();
          });
        } else {
          $scope.login = false;
          $scope.auth = function(){
            gapi.auth.authorize({
              client_id: '1004077656902-nifhv9qcgj0modkv9k9jnrouvptn3qhg.apps.googleusercontent.com',
              scope: 'https://www.googleapis.com/auth/youtube.readonly',
              immediate: false
            }, handleAuthResult);
          };
          ShareContent.html($scope.myyoutubeTemplate);
          if(!$scope.$$phase) $scope.$apply();
        }
      };

      gapi.auth.init(function() {
        gapi.auth.authorize({
          client_id: '1004077656902-nifhv9qcgj0modkv9k9jnrouvptn3qhg.apps.googleusercontent.com',
          scope: 'https://www.googleapis.com/auth/youtube.readonly',
          immediate: true
        }, handleAuthResult);
      });
    });

    $scope.$on('GetEvents', function(){
      $scope.Events = {};
      $scope.active = true;
      ShareContent.html('<div class="progress progress-striped active"><div class="progress-bar"  role="progressbar" style="width: 100%;"></div></div>');
      ShareContent.html($scope.eventTemplate);

      EventsRef.on('child_added', function (EventCategorySnapshot) {

        var category = EventCategorySnapshot.name(), events = EventCategorySnapshot.val();
        var length = _(events).size();
        var count = 0;
        _(events).each( function( event, event_id ) {
          var img = new Image();
          img.src = event.logo;
          imagesLoaded(img, function () {
            if(++count !== length) return;
            $scope.Events[category] = events;
            if(!$scope.$$phase) $scope.$apply();
          });
        });

        (function(category,EventCategorySnapshot){
          EventCategorySnapshot.ref().on('child_added', function(EventSnapshot){
            var event_id = EventSnapshot.name(), event = EventSnapshot.val();
            if(_($scope.Events[category]).isUndefined()) $scope.Events[category] = {};
            if($scope.Events[category][event_id]) return;
            var img = new Image();
            img.src = event.logo;
            imagesLoaded(img, function () {
              $scope.Events[category][event_id] = event;
              if(!$scope.$$phase) $scope.$apply();
            });
          });

          EventCategorySnapshot.ref().on('child_removed', function(EventSnapshot){
            var event_id = EventSnapshot.name();
            
            delete $scope.Events[category][event_id];
            if(!$scope.$$phase) $scope.$apply();
          });

          $scope.$on('$destroy', function(){
            EventCategorySnapshot.ref().off('child_added');
            EventCategorySnapshot.ref().off('child_removed');
          });
        }(category,EventCategorySnapshot));
      });

      EventsRef.on('child_removed', function(EventCategorySnapshot){
        var category = EventCategorySnapshot.name();

        delete $scope.Events[category];
        if(!$scope.$$phase) $scope.$apply();
      });

      $scope.$on('$destroy', function(){
        EventsRef.off('child_removed');
        EventsRef.off('child_added');
      });
    });
  }
]).controller('StageCtrl', [
  '$scope',
  'pubsub',
  '$http',
  'RTCcollection',
  '$rootScope',
  'EventsRef',
  '$compile',
  'FB',
  function ($scope, pubsub, $http, RTCcollection, $rootScope, EventsRef, $compile, FB) {
    var Stage = angular.element('div.stage');
    var player = null;
    var clearStage = function (manually) {
      pubsub.clearChannels('single');
      var video = Stage.find('video')[0];
      if (video) {
        video.pause();
        video.src = '';
      }
      
      if(player) {
        player.stopVideo();
        player.clearVideo();
        player.destroy();
        player = null;
      }
      
      Stage.find('*:not("i.clearStage")').unbind().remove();
      if (manually) {
        pubsub.publish('sync', 'Stage', 'clean', null);
        $rootScope.$broadcast('Sharer', null);
      }
      $scope.clearStage = function(){return false;};
    };

    $scope.clearStage = function(){return false;};

    var LoadingSnippet,VideoLoadingSnippet, EventDetails, CreateEventModal;

    $http({
      method: 'GET',
      url: 'dist/circularG.html'
    }).success(function (data, status, headers, config) {
      LoadingSnippet = data;
    });
    $http({
      method: 'GET',
      url: 'dist/f_circleG.html'
    }).success(function (data, status, headers, config) {
      VideoLoadingSnippet = data;
    });
    
    var youtube_sync = true;
    var youtube_end = false;
    var sync_status = true;
    var onStateChange = function(e){
      
      if(sync_status) {
        switch(e.data){
          case 0:
            $rootScope.$broadcast('SyncState', "icon-stop");
            break;
          case 1:
            $rootScope.$broadcast('SyncState', "icon-play");
            break;
          case 2:
            $rootScope.$broadcast('SyncState', "icon-pause");
            break;
          default:
            break;
        }
      }

      if(!e.data) {
        sync_status = false;
        e.target.stopVideo(); 
        setTimeout(function(){
          youtube_sync = true;
        },0);
        youtube_end = true;
        return;
      }

      if(!youtube_sync) return;
      if(e.data === 1) {
        pubsub.publish('sync', 'single', e.data, e.target.getCurrentTime(), Date.now());
        youtube_sync = false;
      }
      
      if(e.data === 2 && !youtube_end) {
        pubsub.publish('sync', 'single', e.data, e.target.getCurrentTime(), Date.now());
      }
      else if(youtube_end) {
        youtube_end = false;
        sync_status = true;
      }
    };

    var onPlayerReady = function(e){
      youtube_sync = sync_status = false;
      
      pubsub.subscribe('single', function (who, type, currentTime, timestamp) {
        if(!youtube_sync) return;
        //var diff_time = Math.abs(parseInt((Date.now() - timestamp)/1000,10)/100);
        //currentTime = currentTime + diff_time;
        if(type == 1){
          youtube_sync = false;
          if(currentTime !== e.target.getCurrentTime()){
            e.target.seekTo(currentTime, true);
          }
          e.target.playVideo();
        }
        if(type == 2 && !youtube_end){
          if(currentTime !== e.target.getCurrentTime()){
            e.target.seekTo(currentTime, true);
          }
          e.target.pauseVideo();
        }
        else if(youtube_end){
          youtube_end = false;
        }
      });
      var users = angular.element('div.users').scope().users;
      $rootScope.$broadcast('SyncState', 'icon-ok');
      youtube_sync = sync_status = true;
    };

    var generator = {
        event: function(event_data){
          var scope = $rootScope.$new(true);

          $http({
            method: 'GET',
            url: 'dist/EventDetails.html'
          }).success(function (data, status, headers, config) {
            EventDetails = $compile(data)(scope);

             var category = event_data[0], event_id = event_data[1];
             EventsRef.child(category).child(event_id).once('value', function(EventSnapshot){
              var users = angular.element('div.users').scope().users;
              scope = angular.extend(scope, EventSnapshot.val());
              scope.CanNotSubmit = true;
              scope.CanCreate = _(_(users).values()).pluck('sharer').indexOf(true) < 0;
              $http({
                method: 'GET',
                url: 'dist/CreateEventModal.html'
              }).success(function (data, status, headers, config){
                CreateEventModal = $compile(data)(scope);
                
                scope.close = function(){
                  scope.address = scope.events[0].address;
                  scope.datetime = null;
                  scope.CanNotSubmit = true;
                  $(CreateEventModal).modal('hide');
                  if(!$scope.$$phase) $scope.$apply();
                };

                scope.create = function(){
                  if(!scope.datetime && !scope.address) return;
                  Stage.append(VideoLoadingSnippet);
                  
                  FB.api('/me/events','post', {
                    name: scope.name,
                    start_time: scope.datetime+'+0800',
                    description: scope.summary+',詳細資料:'+scope.website,
                    location: scope.address.match(/\(\S+\)/g)[0].replace(/[\(|\)]/g,''),
                    privacy_type: 'OPEN',
                  }, function(response){
                    if(response.error) {
                      console.log(response.error);
                      location.href = '/logout';
                      return;
                    }
                    var event_id = response.id;
                    
                    FB.api('/'+event_id+'/invited','post',{users: _(users).keys().join(',')}, function(response){
                      pubsub.publish('sendSingle','announcement','活動建立成功,可至<a target="_blank" href=https://www.facebook.com/events/'+event_id+'>此處</a>查看');
                      var floatingCirclesG = Stage.find('div.floatingCirclesG');
                      if(floatingCirclesG[0]) {floatingCirclesG.remove();}
                    });
                  });
                  scope.close();
                };

                imagesLoaded(EventDetails, function () {
                  Stage.find('div#circularG').remove();
                  Stage.append(EventDetails);
                  Stage.append(CreateEventModal);
                  $rootScope.$broadcast('SyncState', 'icon-ok');
                  scope.close();
                });
              });
            });
          });
        },
        youtube: function(VideoId){
          $('<div id="ytplayer">').appendTo(Stage);
          Stage.find('div#circularG').remove();
          Stage.append(VideoLoadingSnippet);
          player = new YT.Player('ytplayer', {
            height: Stage.height(),
            width: Stage.width(),
            videoId: VideoId,
            events: {
              'onReady': onPlayerReady,
              'onStateChange': onStateChange
            }
          });
        },
        image: function (src) {
          var Img = new Image();
          Img.src = src;
          imagesLoaded(Img, function () {
            Stage.find('div#circularG').remove();
            Stage.append(Img);
            $rootScope.$broadcast('SyncState', 'icon-ok');
          });
        },
        video: function (src) {
          var Img = new Image();
          Img.src = src[1];
          imagesLoaded(Img, function () {
            var video = angular.element('<video>');
            video.attr({
              poster: src[1],
              src: src[0],
              id: 'video'
            });
            video.bind('loadstart', function () {
              Stage.find('div#circularG').remove();
              Stage.append(video[0]);
              Stage.append(VideoLoadingSnippet);
            });
            video.bind('canplay', function () {
              var users = angular.element('div.users').scope().users;

              if(_(users).isEmpty()) $scope.$emit('allReady');

              pubsub.subscribe('single', function (who, type, currentTime, timestamp) {
                //var diff_time = parseInt((Date.now() - timestamp)/1000,10);
                //currentTime = currentTime + diff_time;
                video.trigger(type, [currentTime]);
              });

              video.bind('play pause ended', function (event, currentTime) {
                if(/^ended$/i.test(event.type)){
                  $rootScope.$broadcast('SyncState', "icon-stop");
                }
                else {
                  $rootScope.$broadcast('SyncState', "icon-"+event.type);  
                }
                
                if (window.stream) {
                  if (/^play$/i.test(event.type)) {
                    $rootScope.$broadcast('audio', false);
                  } else if (/^pause$/i.test(event.type)) {
                    $rootScope.$broadcast('audio', true);
                  }
                }
                if(currentTime) {
                  video[0].currentTime = currentTime;
                }
                else if(/^play|pause$/i.test(event.type)) {
                  pubsub.publish('sync', 'single', event.type, event.target.currentTime, Date.now());
                }
              });
              $rootScope.$broadcast('SyncState', 'icon-ok');
            });
            video[0].load();
          });
        }
      };
    $scope.$on('share', function (event, type, src) {
      clearStage(false);
      Stage.append(LoadingSnippet);
      pubsub.publish('sync', 'Stage', type, src);
      $rootScope.$broadcast('SyncState', 'icon-refresh');
      $rootScope.$broadcast('Sharer', null);
      generator[type](src);
    });
    $scope.$on('allReady', function(){
      $scope.clearStage = clearStage;
      var video = Stage.find('video');
      if(video[0]) {
        video.attr({ controls: true });
      }
      var floatingCirclesG = Stage.find('div.floatingCirclesG');
      if(floatingCirclesG[0]) {floatingCirclesG.remove();}
    });
    pubsub.subscribe('Stage', function (who, type, src) {
      clearStage(false);
      if (type == 'clean') {
        $rootScope.$broadcast('Sharer', null);
        return;
      }
      
      $rootScope.$broadcast('Sharer', who);
      Stage.append(LoadingSnippet);
      $rootScope.$broadcast('SyncState', 'icon-refresh');
      generator[type](src);
    });
  }
]);;
angular.module('IPinYoApp').directive('ngMuted', [
  '$compile',
  function ($compile) {
    return {
      restrict: 'A',
      link: function (scope, element, attrs) {
        attrs.$observe('ngMuted', function (muted) {

          if(muted == 'true') attrs.$set('muted', true);
          else element.removeAttr('muted');

          if(!scope.$$phase) scope.$apply();
        });
      }
    };
  }
]);;
angular.module('IPinYoApp')
  .filter('ASCII', function () {
    'use strict';
    return function (input,pattern) {
      return input.replace(/[\.\/\$#\[\]\s]/g,function(match,position,origin){return '0x'+match.charCodeAt(0);});
    };
  });
;
angular.module('IPinYoApp').filter('CanNotSelect', function () {
  return function (input, item, private) {
    input = _(input).omit('Age');
    input = _.flatten(_.values(input));
    return input.length >= 5 && input.indexOf(item) == -1 || private;
  };
});;
angular.module('IPinYoApp').filter('IsEmpty', function () {
  return function (input, reverse) {
    reverse = reverse || false;
    return reverse ? !_.isEmpty(input) : _.isEmpty(input);
  };
});;
angular.module('IPinYoApp').filter('Size', function () {
  return function (input) {
    return _(_(input).omit('$$hashKey')).size();
  };
});;
angular.module('IPinYoApp').filter('ToChinese', function () {
  return function (input) {
    if (/^interest[s]?$/i.test(input))
      return '興趣';
    else if (/^location$/i.test(input))
      return '地區';
    else if (/^groups$/i.test(input))
      return '社團';
    else if (/^bio$/i.test(input))
      return '關於我';
    else if (/^age$/i.test(input))
      return '年齡';
    else if(/^events$/i.test(input))
      return '公開活動';
    else
      return input;
  };
});;
angular.module('IPinYoApp').filter('TopicFilter', function () {
  return function (teams, search) {
    if (_.isEmpty(search))
      return teams;
    var _teams = {};
    angular.forEach(teams, function (team, teamId) {
      var flags = [];
      angular.forEach(search, function (topics, title) {
        if (_.isUndefined(team.topics[title])){
          flags.push(false);
          return;
        }
        if (/^age$/i.test(title)) {
          var range = _.range(topics.down, topics.up + 1, 1);
          var team_range = _.range(team.topics[title].down, team.topics[title].up + 1, 1);
          if (_.intersection(team_range, range).length) {
            flags.push(true);
          } else {
            flags.push(false);
          }
        } else if (topics.length === _.intersection(team.topics[title], topics).length) {
          flags.push(true);
        } else {
          flags.push(false);
        }
      });
      
      if (_.isEmpty(flags) || flags.indexOf(false) >= 0)
        return;
      _teams[teamId] = team;
    });
    return _teams;
  };
});;
angular.module('IPinYoApp')
  .filter('UnitTransfer', function () {
    'use strict';
    return function (input) {
        if(input/100000000 >= 1)
            return Math.round(input/10000000)/10 + 'B';
        else if(input/1000000 >= 1)
            return Math.round(input/100000)/10 + 'M';
        else if(input/1000 >= 1)
            return Math.round(input/100)/10 + 'K';
        else if(!input)
            return '?';
        else
            return input;
    };
  });
;
angular.module('IPinYoApp').filter('arrayJoin', function () {
  return function (input, delimeter) {
    return input.join(delimeter);
  };
});;
angular.module('IPinYoApp').filter('genderIcon', function () {
  return function (input, type) {
    if (type == 'gender')
      return input == 'male' ? 'icon-male male' : 'icon-female female';
    else if (type == 'status')
      return input == 'Online' ? 'icon-circle-blank text-success' : 'icon-circle text-info link';
    else
      return input;
  };
});;
angular.module('IPinYoApp').filter('lineLight', function () {
  return function (input, search) {
    if (_.isUndefined(input))
      return;
    if (_.isArray(input)) {
      return input.indexOf(search) < 0 ? '' : 'selected';
    } else {
      var range = _.range(input.down, input.up + 1, 1);
      var room_range = _.range(search.down, search.up + 1, 1);
      if (_.intersection(range, room_range).length == room_range.length) {
        return 'selected';
      } else if (_.intersection(range, room_range).length) {
        return 'partial selected';
      } else {
        return '';
      }
    }
  };
});;
angular.module('IPinYoApp').filter('teamFull', function () {
  return function (teams) {
    if (!teams)
      return teams;
    var _teams = {};
    angular.forEach(teams, function (team, teamId) {
      if (team.members && _.keys(team.members).length < 4) {
        _teams[teamId] = team;
      }
    });
    return _teams;
  };
});;
angular.module('IPinYoApp').service('EventsRef', function () {
  var EventsRef = new Firebase('https://grassshrimp.firebaseio.com/Events');
  this.auth = function (token) {
    EventsRef.auth(token);
  };
  return EventsRef;
});;
angular.module('IPinYoApp').factory('FB', function () {
    FB.init({
        appId: '517132108346068',
        channelUrl: '//www.ipinyo.com/channel.html',
        status: true,
        xfbml: true,
        cookie: true,
        oauth: true
    });
    
    FB.Event.subscribe('auth.logout', function(response) {
        location.href = '/logout';
    });

    return FB;
});;
angular.module('IPinYoApp')
  .service('FiltersRef', function () {
    'use strict';
    var FiltersRef = new Firebase('https://grassshrimp.firebaseio.com/Filters');

    this.auth = function (token) {
        FiltersRef.auth(token);
    };

    return FiltersRef;
  });
;
angular.module('IPinYoApp').factory('Me', [
  '$q',
  'FB',
  '$rootScope',
  function Me($q, FB, $rootScope) {
    return function () {
      var defer = $q.defer();
      var fields = Array.prototype.splice.call(arguments, 0);
      FB.getLoginStatus(function (response) {
        FB.api('/me', { fields: fields.join(',') }, function (response) {
          if (response.error) {
            console.log(response.error);
            location.href = '/logout';
            return;
          } else {
            if (response.birthday) {
              response.age = function (birthday) {
                birthday = new Date(birthday);
                return parseInt((Date.now() - birthday.valueOf()) / 31557600000,10);
              }(response.birthday);
            }
            defer.resolve(response);
          }
          if(!$rootScope.$$phase) $rootScope.$apply();
        });
      });
      return defer.promise;
    };
  }
]);;
angular.module('IPinYoApp').service('OnlineUserRef', function () {
  var OnlineUserRef = new Firebase('https://grassshrimp.firebaseio.com/OnlineUser');
  this.auth = function (token) {
    OnlineUserRef.auth(token);
  };
  return OnlineUserRef;
});;
angular.module('IPinYoApp').service('PassageWay', [
  'pubsub',
  '$rootScope',
  'TeamsRef',
  'OnlineUserRef',
  'TopicsRef',
  'FiltersRef',
  'PrivateTeamsRef',
  'EventsRef',
  function (pubsub, $rootScope, TeamsRef, OnlineUserRef, TopicsRef, FiltersRef, PrivateTeamsRef, EventsRef) {
    var socket = '';
    var is_connected = false;
    var sendSingle = function (e) {
      socket.emit.apply(socket, arguments);
    };
    var reciveSingle = function (e) {
      pubsub.publish.apply(null, arguments);
    };
    this.IsConnect = function(){
      return is_connected;
    };
    this.connect = function (callback) {
      socket = io.connect(location.origin, { reconnect: false, secure: true });
      socket.on('connect', function (e) {
        socket.on('token', function (token) {
          PrivateTeamsRef.auth(token, function(){
            is_connected = true;
          });
          TeamsRef.auth(token,function(){
            is_connected = true;
          });
          OnlineUserRef.auth(token, function(){
            is_connected = true;
          });
          TopicsRef.auth(token, function(){
            is_connected = true;
          });
          FiltersRef.auth(token, function(){
            is_connected = true;
          });

          EventsRef.auth(token, function(){
            is_connected = true;
          });
        });
        socket.emit('getToken');
        pubsub.subscribe('sendSingle', sendSingle);
        socket.on('reciveSingle', reciveSingle);
        socket.on('disconnect', function () {
          location.href="/logout";
          return;
        });
        console.log('connect successfully');
        if (typeof callback === 'function') {
          callback();
        }
      });
    };
  }
]);;
angular.module('IPinYoApp').service('PrivateTeamsRef', function () {
  var PrivateTeamsRef = new Firebase('https://grassshrimp.firebaseio.com/PrivateTeams');
  this.auth = function (token) {
    PrivateTeamsRef.auth(token);
  };
  return PrivateTeamsRef;
});;
angular.module('IPinYoApp').factory('RTC', [
  'pubsub',
  '$rootScope',
  function (pubsub, $rootScope) {
    return function (remote, active, iceServers) {
      
      var pc_config = {
          'iceServers': iceServers
        }, sdpConstraints = {
          'mandatory': {
            'OfferToReceiveAudio': true,
            'OfferToReceiveVideo': true
          }
        }, RTCPeerConnection = window.mozRTCPeerConnection || window.webkitRTCPeerConnection, RTCIceCandidate = window.mozRTCIceCandidate || window.RTCIceCandidate, RTCSessionDescription = window.mozRTCSessionDescription || window.RTCSessionDescription, pc = null, dc = null, closeFakeDataChannel = null,
        onIceCandidate = function (e) {
          if (_.isNull(e.candidate))
            return;
          pubsub.publish('sendSingle', 'sendIceCandidate', remote, e.candidate);
          pc.onicecandidate = null;
        }, onAddStream = function (e) {
          $rootScope.$broadcast('addStream', remote, e.stream);
        }, onRemoveStream = function (e) {
        }, sendAnswer = function () {
          pc.createAnswer(function (desc) {
            pc.setLocalDescription(desc);
            pubsub.publish('sendSingle', 'sendAnswer', remote, desc);
          }, function(error){console.log(error);}, sdpConstraints);
        }, preferOpus = function (sdp) {
          var sdpLines = sdp.split('\r\n');
          var mLineIndex = null;
          for (var i = 0; i < sdpLines.length; i++) {
            if (sdpLines[i].search('m=audio') !== -1) {
              mLineIndex = i;
              break;
            }
          }
          if (mLineIndex === null)
            return sdp;
          for (i = 0; i < sdpLines.length; i++) {
            if (sdpLines[i].search('opus/48000') !== -1) {
              var opusPayload = extractSdp(sdpLines[i], /:(\d+) opus\/48000/i);
              if (opusPayload)
                sdpLines[mLineIndex] = setDefaultCodec(sdpLines[mLineIndex], opusPayload);
              break;
            }
          }
          sdpLines = removeCN(sdpLines, mLineIndex);
          sdp = sdpLines.join('\r\n');
          return sdp;
        }, addStereo = function (sdp) {
          var sdpLines = sdp.split('\r\n');
          var opusPayload = '';
          for (var i = 0; i < sdpLines.length; i++) {
            if (sdpLines[i].search('opus/48000') !== -1) {
              opusPayload = extractSdp(sdpLines[i], /:(\d+) opus\/48000/i);
              break;
            }
          }
          var fmtpLineIndex = null;
          for (i = 0; i < sdpLines.length; i++) {
            if (sdpLines[i].search('a=fmtp') !== -1) {
              var payload = extractSdp(sdpLines[i], /a=fmtp:(\d+)/);
              if (payload === opusPayload) {
                fmtpLineIndex = i;
                break;
              }
            }
          }
          if (fmtpLineIndex === null)
            return sdp;
          sdpLines[fmtpLineIndex] = sdpLines[fmtpLineIndex].concat(' stereo=1');
          sdp = sdpLines.join('\r\n');
          return sdp;
        }, createFakeDataChannel = function(){
          var pubsub_receiveMessage = pubsub.subscribe('receiveMessage', function(remote, datatype, message, timestamp){
            pubsub.publish('message', remote, datatype, message, timestamp);
          });

          var pubsub_receiveStage = pubsub.subscribe('receiveStage', function(remote, datatype, src, timestamp){
            pubsub.publish('Stage', remote, datatype, src, timestamp);
          });

          var pubsub_receiveSingle = pubsub.subscribe('receiveSingle', function(remote, actiontype, currentTime, timestamp){
            pubsub.publish('single', remote, actiontype, currentTime, timestamp);
          });

          var pubsub_receiveState = pubsub.subscribe('receiveState', function(remote, type, data, timestamp){
            pubsub.publish('State', remote, type, data, timestamp);
          });

          $rootScope.$broadcast('addStream', remote, null);
          
          var pubsub_sync = pubsub.subscribe('sync', function (EventType, DataType, Data, TimeStamp, Specific) {
            if(Specific && Specific != remote) return;

            pubsub.publish('sendSingle',EventType, remote, DataType, Data, TimeStamp);
          });

          closeFakeDataChannel = function(){
            pubsub.unsubscribe(pubsub_sync);
            pubsub.unsubscribe(pubsub_receiveMessage);
            pubsub.unsubscribe(pubsub_receiveStage);
            pubsub.unsubscribe(pubsub_receiveSingle);
            pubsub.unsubscribe(pubsub_receiveState);
          };
        }, createDataChannel = function () {
          var pubsub_sync = null, pubsub_addstream = null;

          dc.onmessage = function (e) {
            var data = JSON.parse(e.data);
            pubsub.publish(data.EventType, remote, data.DataType, data.Data, data.TimeStamp);
          };
          dc.onopen = function (e) {
            closeFakeDataChannel();

            pubsub_sync = pubsub.subscribe('sync', function (EventType, DataType, Data, TimeStamp, Specific) {
              if(Specific && Specific != remote) return;

              var data = {
                  EventType: EventType,
                  DataType: DataType,
                  Data: Data,
                  TimeStamp: TimeStamp
                };
              dc.send(JSON.stringify(data));
            });
            
            if (window.stream) {
              var audioTracks = window.stream.getAudioTracks()[0];
              var videoTracks = window.stream.getVideoTracks()[0];
              if (audioTracks)
                pubsub.publish('sync', 'State', 'audio', audioTracks.enabled);
              else
                pubsub.publish('sync', 'State', 'audio', false);

              if(videoTracks)
                pubsub.publish('sync', 'State', 'video', videoTracks.enabled);
              else
                pubsub.publish('sync', 'State', 'video', false);
            }

            dc.onclose = function(){
              if(pubsub_addstream) pubsub.unsubscribe(pubsub_addstream);
              if(pubsub_sync) pubsub.unsubscribe(pubsub_sync);
            };
          };
        }, extractSdp = function (sdpLine, pattern) {
          var result = sdpLine.match(pattern);
          return result && result.length == 2 ? result[1] : null;
        }, removeCN = function (sdpLines, mLineIndex) {
          var mLineElements = sdpLines[mLineIndex].split(' ');
          for (var i = sdpLines.length - 1; i >= 0; i--) {
            var payload = extractSdp(sdpLines[i], /a=rtpmap:(\d+) CN\/\d+/i);
            if (payload) {
              var cnPos = mLineElements.indexOf(payload);
              if (cnPos !== -1) {
                mLineElements.splice(cnPos, 1);
              }
              sdpLines.splice(i, 1);
            }
          }
          sdpLines[mLineIndex] = mLineElements.join(' ');
          return sdpLines;
        }, setDefaultCodec = function (mLine, payload) {
          var elements = mLine.split(' ');
          var newLine = [];
          var index = 0;
          for (var i = 0; i < elements.length; i++) {
            if (index === 3)
              newLine[index++] = payload;
            if (elements[i] !== payload)
              newLine[index++] = elements[i];
          }
          return newLine.join(' ');
        }, createPeerConnection = function () {
          pc = new RTCPeerConnection(pc_config, {
            optional: [
              { DtlsSrtpKeyAgreement: true },
              { RtpDataChannels: true }
            ]
          });
          angular.extend(pc, {
            onicecandidate: onIceCandidate,
            onaddstream: onAddStream,
            onremovestream: onRemoveStream
          });          
          createFakeDataChannel();

          if(active) {
            dc = pc.createDataChannel('grassshrimp-webrtc', {});

            createDataChannel();
          }
          else {
            pc.ondatachannel = function (e) {
                dc = e.channel;
                createDataChannel();
            };
          }
        };
      createPeerConnection();
      this.addStream = function (stream) {
        pc.addStream(stream);
      };
      this.sendOffer = function () {
        pc.createOffer(function (desc) {
          pc.setLocalDescription(desc);
          pubsub.publish('sendSingle', 'sendOffer', remote, desc);
        }, function(error){console.log(error);}, sdpConstraints);
      };
      this.addIceCandidate = function (candidate) {
        if (candidate)
          pc.addIceCandidate(new RTCIceCandidate(candidate));
      };
      this.receiveOffer = function (desc) {
        pc.setRemoteDescription(new RTCSessionDescription(desc));
        sendAnswer();
      };
      this.receiveAnswer = function (desc) {
        pc.setRemoteDescription(new RTCSessionDescription(desc));
      };
      this.disconnect = function () {
        if(closeFakeDataChannel) closeFakeDataChannel();
        if(window.stream) pc.removeStream(window.stream);
        if(pc) pc.close();
        if(dc) dc.close();
      };
    };
  }
]);;
angular.module('IPinYoApp').service('RTCcollection', [
  'pubsub',
  'RTC',
  function (pubsub, RTC) {
    var collection = {}, findRTC = function (remote) {
        return collection[remote];
      };
    pubsub.subscribe('receiveIceCandidate', function (remote, candidate) {
      findRTC(remote).addIceCandidate(candidate);
    });
    pubsub.subscribe('receiveOffer', function (remote, sdp) {
      findRTC(remote).receiveOffer(sdp);
    });
    pubsub.subscribe('receiveAnswer', function (remote, sdp) {
      findRTC(remote).receiveAnswer(sdp);
    });
    this.CreateRTC = function (remote, active, iceServers) {
      var rtc = new RTC(remote, active, iceServers);
      if(window.stream) rtc.addStream(window.stream);
      collection[remote] = rtc;
      if(active) collection[remote].sendOffer();
    };
    this.RemoveRTC = function (remote) {
      if(collection[remote]) collection[remote].disconnect();
      delete collection[remote];
    };
    this.RemoveAll = function () {
      _.each(collection, function (rtc) {
        rtc.disconnect();
      });
      collection = {};
    };
    this.GetAll = function () {
      return collection;
    };
  }
]);;
angular.module('IPinYoApp').factory('Stream', [
  '$q',
  '$rootScope',
  '$http',
  '$compile',
  function Stream($q, $rootScope, $http, $compile) {
    return function () {
      var defer = $q.defer();
      /*
      if (_.isUndefined(window.stream)) {
        $http({
          method: 'GET',
          url: 'dist/NoticeModal.html'
        }).success(function (data, status, headers, config) {
          var scope = $rootScope.$new(true);
          var NoticeModal = '';
          scope.close = function () {
            $(NoticeModal).modal('hide');
          };
          NoticeModal = $compile(data)(scope);
          if(_.isUndefined(angular.element('#NoticeModal')[0])) angular.element('body').append(NoticeModal);
          navigator.getUserMedia({
            video: true,
            audio: true
          }, function (stream) {
            window.stream = stream;
            scope.close();
            defer.resolve(stream);
            if(!$rootScope.$$phase) $rootScope.$apply();
          }, function (err) {
            window.stream = null;
            scope.close();
            defer.resolve(null);
            if(!$rootScope.$$phase) $rootScope.$apply();
          });
          $(NoticeModal).modal('show');
        });
      } else {
        defer.resolve(window.stream);
        if(!$rootScope.$$phase) $rootScope.$apply();
      }
      */
      window.stream = null;
      defer.resolve(null);
      return defer.promise;
    };
  }
]);;
angular.module('IPinYoApp').service('TeamsRef', function () {
  var TeamsRef = new Firebase('https://grassshrimp.firebaseio.com/Teams');
  this.auth = function (token) {
    TeamsRef.auth(token);
  };
  return TeamsRef;
});;
angular.module('IPinYoApp')
  .service('TopicsRef', function () {
    'use strict';
    var TopicsRef = new Firebase('https://grassshrimp.firebaseio.com/Topics');

    this.auth = function (token) {
        TopicsRef.auth(token);
    };

    return TopicsRef;
  });
;
angular.module('IPinYoApp').factory('arrayToggle', function () {
  return function (array, item) {
    var index = array.indexOf(item);
    if (index === -1) {
      array.push(item);
      return true;
    } else {
      array.splice(index, 1);
      return false;
    }
  };
});;
angular.module('IPinYoApp').service('pubsub', function () {
  var pubsub = {};
  this.publish = function () {
    var args = Array.prototype.slice.apply(arguments);
    var topic = args.shift();
    if(pubsub[topic]){
      $.each(pubsub[topic], function () {
        args = args || [];
        this.apply(null, args);
      });
    }
  };
  this.subscribe = function (topic, callback) {
    if (!pubsub[topic]) {
      pubsub[topic] = [];
    }
    pubsub[topic].push(callback);
    return [
      topic,
      callback
    ];
  };
  this.unsubscribe = function (handle) {
    var t = handle[0];
    if(pubsub[t]){
      $.each(pubsub[t], function (idx) {
        if (this == handle[1]) {
          pubsub[t].splice(idx, 1);
        }
      });
    }
    return this;
  };
  this.clearChannels = function () {
    var channels = Array.prototype.slice.apply(arguments);
    $.each(channels, function (index, channel) {
      pubsub[channel] = null;
      delete pubsub[channel];
    });
    return this;
  };
  this.getPubSub = function () {
    return pubsub;
  };
});